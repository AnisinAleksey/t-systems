import '../css/breadcrumbs.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );
require( 'jquery.cookie' );

export var CargoesTable = React.createClass({
    propTypes: {
        cargoes: React.PropTypes.array.isRequired,
    },
    deleteCargo: function(row){
        var cargoArray = this.props.cargoes;
        function CargoObject(name, weight, load, unload){
            this.name = name;
            this.weight = weight;
            this.load = load;
            this.unload = unload
        }
        var elem = cargoArray.filter((el) => {
           return el.name == row.name && el.weight == row.weight &&
               el.load.city == row.load && el.unload.city == row.unload;
        });
        var itemToRemove = new CargoObject(elem.name, elem.weight, elem.load, elem.unload);
        cargoArray.splice($.inArray(itemToRemove, cargoArray),1);
        $.cookie('cargoes', JSON.stringify(cargoArray));
        this.setState({cargoes: cargoArray}, function () {
            ReactDOM.render(<CargoesTable cargoes ={this.props.cargoes}/>, document.getElementById("cargoesTableDiv"));
        })
    },
    handleDeleteRow(next, rowKeys){
        next.call();
    },
    render: function() {
        var selectRowProp = {
            mode: "radio",
            clickToSelect: true,
            bgColor: "rgb(238, 193, 213)",
            onSelect: this.setSelectRow
        };
        var options = {
            onDeleteRow: this.deleteCargo,
            handleConfirmDeleteRow : this.handleDeleteRow,
        };
        function Cargo(name, weight, load, unload){
            this.name = name;
            this.weight = weight;
            this.load = load;
            this.unload = unload
        }
        var cargoArray = [];
        this.props.cargoes.forEach((elem) => {
            cargoArray.push(new Cargo(elem.name,elem.weight,elem.load.city,elem.unload.city));
        });
        return (
            <BootstrapTable search={true} data={cargoArray} striped={true} hover={true} deleteRow={true} selectRow={selectRowProp} options ={options}>
                <TableHeaderColumn dataField="name" isKey={true} dataAlign="center">Name</TableHeaderColumn>
                <TableHeaderColumn dataField="weight" dataAlign="center">Weight</TableHeaderColumn>
                <TableHeaderColumn dataField="load" dataAlign="center">Load</TableHeaderColumn>
                <TableHeaderColumn dataField="unload" dataAlign="center">Unload</TableHeaderColumn>
            </BootstrapTable>
        );
    }
});
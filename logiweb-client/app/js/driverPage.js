import '../css/driverPageStyles.css';
import '../css/bootstrap.min.css';
import '../css/react-bootstrap-table-all.min.css';

import {GoogleMapMarkers} from './GoogleMap.js';

var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');
global.jQuery = $;
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
require('bootstrap');


var DriverInfo = React.createClass({
    getInitialState() {
        return {
            personalNumber: "",
            vin: "",
            orderNumber: "",
            coDriversArray: [],
            waypointArray: []
        };
    },
    componentWillMount: function () {
        var personalNumberTemp = "";
        var vinTemp = "";
        var orderNumberTemp = "";
        var coDriversArrayTemp = [];
        var waypointArrayTemp = [];
        $.ajax({
            url: "/driver/driversInfo",
            async: false,
            method: 'GET',
            success: function (data) {
                var jsonData = JSON.parse(data);
                personalNumberTemp = jsonData.p_number;
                vinTemp = jsonData.regnumber;
                orderNumberTemp = jsonData.number;
                coDriversArrayTemp = jsonData.coDrivers;
                waypointArrayTemp = jsonData.waypoints;
            }
        });
        this.setState(
            {
                personalNumber: personalNumberTemp,
                vin: vinTemp,
                orderNumber: orderNumberTemp,
                coDriversArray: coDriversArrayTemp,
                waypointArray: waypointArrayTemp
            }
        );
    },
    render: function () {
        var length = this.state.waypointArray.length;
        return (
            <div>
                <p className="logout" id="logout"><a href="/logout">Logout</a></p>
                <label className="driverLabel">Personal number:</label>
                <input className="driverField" type="text" contentEditable="false" value={this.state.personalNumber}/>
                <br/>
                <label className="driverLabel">Vin of truck</label>
                <input className="driverField" type="text" contentEditable="false" value={this.state.vin}/>
                <br/>
                <label className="driverLabel">Order</label>
                <input className="driverField" type="text" contentEditable="false" value={this.state.orderNumber}/>
                <br/>
                <div>
                    <label className="driverLabel">Co-drivers</label>
                    <BootstrapTable data={this.state.coDriversArray}>
                        <TableHeaderColumn dataField="p_number" isKey={true} dataAlign="center" dataSort={true}>Personal
                            Number</TableHeaderColumn>
                    </BootstrapTable>
                </div>
                <div>
                    <label className="driverLabel">Waypoints</label>
                    {(() => {
                        if(length != 0) {
                            return <GoogleMapMarkers waypoints={this.state.waypointArray}
                                width = "500px" height="300px"
                                zoom={5}
                                center={{lat: 51.0447817, lon: 11.265354 }} />;
                        }
                    })()}

                    <BootstrapTable data={this.state.waypointArray} striped={true}>
                        <TableHeaderColumn dataField="city" isKey={true}>City</TableHeaderColumn>
                    </BootstrapTable>
                </div>
            </div>
        )
    }
});


ReactDOM.render(<DriverInfo/>, document.getElementById("mainDriverDiv"));


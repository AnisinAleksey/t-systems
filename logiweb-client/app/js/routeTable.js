import '../css/breadcrumbs.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );
require( 'jquery.cookie' );

export var RouteTable = React.createClass({
    propTypes: {
        cityData: React.PropTypes.array.isRequired,
    },
    deleteCityFromRoute: function(row){
      var cityArray = this.props.cityData;
        function RouteElement(city) {
            this.city =city;
        }
        var itemToRemove = new RouteElement(row.city);
        cityArray.splice($.inArray(itemToRemove, cityArray),1);
        $.cookie('route', JSON.stringify(cityArray));
        this.setState({cityData: cityArray}, function () {
            ReactDOM.render(<RouteTable cityData ={this.props.cityData}/>, document.getElementById("routeTableDiv"));
        })
    },
    handleDeleteRow(next, rowKeys){
        next.call();
    },
    render: function() {
        var selectRowProp = {
            mode: "radio",
            clickToSelect: true,
            bgColor: "rgb(238, 193, 213)",
            onSelect: this.setSelectRow
        };
        var options = {
            onDeleteRow: this.deleteCityFromRoute,
            handleConfirmDeleteRow : this.handleDeleteRow
        };
        return (
            <BootstrapTable search={true} data={this.props.cityData} striped={true} hover={true} deleteRow={true} selectRow={selectRowProp} options ={options}>
                <TableHeaderColumn dataField="city" isKey={true} dataAlign="center">City</TableHeaderColumn>
            </BootstrapTable>
        );
    }
});
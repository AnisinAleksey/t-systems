import '../css/breadcrumbs.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );
require( 'jquery.cookie' );

export var SelectDriverTable = React.createClass({
    propTypes: {
        drivers: React.PropTypes.array.isRequired,
    },
    render: function() {
        return (
            <BootstrapTable data={this.props.drivers} striped={true} hover={true}>
                <TableHeaderColumn dataField="name" dataSort={true}>Name</TableHeaderColumn>
                <TableHeaderColumn dataField="surname" dataSort={true}>Surname</TableHeaderColumn>
                <TableHeaderColumn dataField="p_number" isKey={true} dataAlign="center" dataSort={true}>Personal Number</TableHeaderColumn>
                <TableHeaderColumn dataField="status" dataSort={true}>Status</TableHeaderColumn>
                <TableHeaderColumn dataField="city" dataSort={true}>City</TableHeaderColumn>
            </BootstrapTable>
        );
    }
});
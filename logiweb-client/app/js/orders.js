import '../css/orders.css';
import '../css/breadcrumbs.css';
import '../css/bootstrap.min.css';
import '../css/react-bootstrap-table-all.min.css';

var React = require('react');
var ReactDOM = require('react-dom');
var Wizard = require('react-wizard');
var $ = require('jquery');
global.jQuery = $;
require( 'jquery.cookie' );
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal';
import Alert from 'react-bootstrap/lib/Alert';
import {BreadCrumbsComponent} from './breadcrumbs';
import {DataListCities} from './dataListCities';
import {RouteStep} from './wizardSteps';
import {CargoStep} from './wizardSteps';
import {TruckStep} from './wizardSteps';
import {DriverStep} from './wizardSteps';
import {OrderStep} from './wizardSteps';
import {SaveStep} from './wizardSteps';
import {FinishStep} from './wizardSteps';
import {RouteTable} from './routeTable';
require('bootstrap');

export var OrdersDataTable = React.createClass({
    getInitialState() {
        return {
            data: [],
            showModal: false,
            waypointData: [],
            driverData: []
        };
    },
    componentWillMount: function () {
        var ordersData = [];
        $.ajax({
            url: "/employee/orders",
            async: false,
            method: 'GET',
            success: function(data) {
                ordersData = JSON.parse(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                    messageText="Unable get trucks"/>, document.getElementById('messageAlertDiv'));
            }
        });
        this.state.data = ordersData;
    },

    setSelectRow: function (row) {
        var waypointsArray = [];
        var driversArray = [];
        $.ajax({
            url: "/employee/orders/details/" + row.number,
            async: false,
            method: 'GET',
            success: function (data) {
                var result = JSON.parse(data);
                waypointsArray = result.waypoints;
                driversArray = result.drivers;
            },
            error: function (jqXHR, textStatus, errorThrown) {}
        });
        ReactDOM.render(<DetailInformation row={row} waypointData={waypointsArray} driverData={driversArray}/>, document.getElementById("detailInformation"));
    },

    deleteOrder: function (row) {
        this.closeConfirm();
        var result = false;
        $.ajax({
            url: "/employee/orders/" + row,
            async: false,
            method: 'DELETE',
            success: function (data) {
                ReactDOM.render(<MessageAlert type="success"
                                              messageText="Order was deleted"/>, document.getElementById('messageAlertDiv'));
                result = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable delete order"/>, document.getElementById('messageAlertDiv'));
            }
        });
        if(result) {
            var orders = this.state.data.filter((order) => {
                return order.number !== row[0];
            });

            this.setState({
                data: orders
            });
            var EmptyComp = React.createClass({
                render() {
                    return (
                        null
                    );
                }
            });
            ReactDOM.render(<EmptyComp/>, document.getElementById("detailInformation"));
        }

    },
    handleDeleteRow: function (next, rowKeys) {
        this.openConfirm(next, rowKeys);
    },
    closeConfirm() {
        this.setState({showModal: false});
    },
    openConfirm(next, rowKeys) {
        this.setState({showModal: true}, function () {
            $("#openConfirmButton").click(function () {
                next();
            });
        });
    },
    numberValidator: function (value) {
        if (!value) {
            return 'Not empty field'
        } else if (!(/^\d+$/.test(value))) {
            return 'Only numbers!'
        }
        return true;
    },
    textValidator: function (value) {
        if (!value) {
            return 'Not empty field'
        } else if (!(/^[a-zA-Z]+$/.test(value))) {
            return 'Only characters!'
        }
        return true;
    },
    showOrderWizard: function () {
        $.removeCookie('route');
        $.removeCookie('cargoes');
        $.removeCookie('truck');
        $.removeCookie('drivers');
        ReactDOM.render(React.createElement(Wizard, {
            steps: stepsWizard,
            something: 'New order',
        }), document.getElementById("detailInformation"));
    },
    render: function () {
        var selectRowProp = {
            mode: "radio",
            clickToSelect: true,
            bgColor: "rgb(238, 193, 213)",
            onSelect: this.setSelectRow
        };
        var options = {
            onDeleteRow: this.deleteOrder,
            handleConfirmDeleteRow: this.handleDeleteRow
        };
        return (
            <div>
                <div id="messageAlertDiv"></div>
                <div>
                    <div id="orderDataTable">
                        <BootstrapTable remote={true} data={this.state.data} striped={true} hover={true} pagination={true}
                                        selectRow={selectRowProp} deleteRow={true} options={options}>
                            <TableHeaderColumn dataField="number" isKey={true} dataSort={true}
                                               editable={{type:'text', validator: this.numberValidator}}>Number</TableHeaderColumn>
                            <TableHeaderColumn dataField="state" dataSort={true}
                                               editable={{type:'select', options:{values:["COMPLETED", "UNCOMPLETED"]}}}>State</TableHeaderColumn>
                            <TableHeaderColumn dataField="truck_id" dataAlign="center"
                                               dataSort={true}>VIN</TableHeaderColumn>
                        </BootstrapTable>
                        <Button
                            bsStyle="success"
                            bsSize="sm"
                            onClick={this.showOrderWizard}>
                            Add
                        </Button>
                    </div>

                    <Modal show={this.state.showModal} onHide={this.closeConfirm} id="confirmDeletingDiv">
                        <Modal.Header closeButton>
                            <Modal.Title>Are you sure?</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Would you like to remove this order?
                        </Modal.Body>
                        <Modal.Footer>
                            <Button
                                bsStyle="success"
                                bsSize="sm"
                                id="openConfirmButton">
                                Yes
                            </Button>
                            <Button
                                bsStyle="danger"
                                bsSize="sm"
                                onClick={this.closeConfirm}>
                                No
                            </Button>
                        </Modal.Footer>
                    </Modal>
                    <div id="detailInformation">
                    </div>
                    <div id="wizardDiv">
                    </div>
                </div>
            </div>
        )
    }
});
const MessageAlert = React.createClass({
    propTypes: {
        messageText: React.PropTypes.string.isRequired,
        type: React.PropTypes.string.isRequired
    },
    render() {
        this.handleAlertShow();
        return (
            <Alert id="alarmingDiv" bsStyle={this.props.type} onDismiss={this.handleAlertDismiss}>
                <p>{this.props.messageText}</p>
            </Alert>
        );
    },
    handleAlertDismiss: function () {
        $("#alarmingDiv").hide();
    },

    handleAlertShow() {
        $("#alarmingDiv").show();
    }
});

var DetailInformation = React.createClass({
    propTypes: {
        waypointData: React.PropTypes.array.isRequired,
        driverData: React.PropTypes.array.isRequired,
        row: React.PropTypes.object.isRequired
    },
    render() {
        return (
            <div>
                <label className="modalLabel">Number</label>
                <input id="number" className="modalField" type="text" placeholder="number"
                       value={this.props.row.number}/>
                <br/>
                <label className="modalLabel">State</label>
                <input id="state" className="modalField" type="text" placeholder="state" value={this.props.row.state}/>
                <br/>
                <label className="modalLabel">Truck</label>
                <input id="truck_id" className="modalField" type="text" placeholder="truck"
                       value={this.props.row.truck_id}/>
                <br/>
                <label className="modalLabel">Waypoint</label>
                <BootstrapTable data={this.props.waypointData} striped={true} id="orderDataTable">
                    <TableHeaderColumn dataField="city" isKey={true}>City</TableHeaderColumn>
                </BootstrapTable>
                <br/>
                <label className="modalLabel">Drivers</label>
                <BootstrapTable data={this.props.driverData} striped={true} id="driverDataTable">
                    <TableHeaderColumn dataField="p_number" isKey={true}>Personal Number</TableHeaderColumn>
                    <TableHeaderColumn dataField="surname">Surname</TableHeaderColumn>
                    <TableHeaderColumn dataField="name">Name</TableHeaderColumn>
                    <TableHeaderColumn dataField="status">Status</TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    },
});


const stepsWizard = [RouteStep, CargoStep, TruckStep, DriverStep, OrderStep, SaveStep, FinishStep];

ReactDOM.render(<BreadCrumbsComponent activeTab='orders'/>, document.getElementById("breadcrumbsDiv"));
ReactDOM.render(<OrdersDataTable/>, document.getElementById("orderDiv"));


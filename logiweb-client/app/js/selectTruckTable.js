import '../css/breadcrumbs.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );
require( 'jquery.cookie' );

export var SelectTruckTable = React.createClass({
    propTypes: {
        truck: ""
    },
    render: function() {
        return (
            <div>
                <label className="wizardHeaderLabel">Choosen truck:</label>
                    <BootstrapTable data={this.props.truck} striped={true}>
                        <TableHeaderColumn dataField="regnumber" isKey={true} dataAlign="center" dataSort={true}>VIN</TableHeaderColumn>
                        <TableHeaderColumn dataField="count_drivers" dataSort={true}>Count</TableHeaderColumn>
                        <TableHeaderColumn dataField="capacity" dataSort={true}>Capacity</TableHeaderColumn>
                        <TableHeaderColumn dataField="state" dataSort={true}>State</TableHeaderColumn>
                        <TableHeaderColumn dataField="city" dataSort={true}>City</TableHeaderColumn>
                    </BootstrapTable>
                </div>
        );
    }
});
"use strict";

import React from 'react';
import {Gmaps, Marker, InfoWindow, Circle} from 'react-gmaps';

export var GoogleMapMarkers = React.createClass({

    propTypes: {
        waypoints: React.PropTypes.array.isRequired,
        center: React.PropTypes.object,
        zoom: React.PropTypes.number,
        width: React.PropTypes.string,
        height: React.PropTypes.string
    },

    getInitialState() {
        return {
            waypoints: this.props.waypoints,
            center: this.props.center,
            zoom: this.props.zoom,
            width: this.props.width,
            height: this.props.height
        };
    },

    onMapCreated(map) {
        map.setOptions({
            disableDefaultUI: true
        });
    },

    render() {
        var points = [];
        this.state.waypoints.map((waypoint,i) => {
            points.push(<Marker
                key={i}
                lat={waypoint.lat}
                lng={waypoint.lon}/>)
        });
        return (
            <Gmaps
                width={this.state.width}
                height={this.state.height}
                lat={this.state.center.lat}
                lng={this.state.center.lon}
                zoom={this.state.zoom}
                loadingMessage={'Be happy'}
                params={{v: '3.exp', key: 'AIzaSyBHjW-5Fst2N3g2I9YscIJ5e3uxhGJBWiA'}}
                onMapCreated={this.onMapCreated}>
                {points}
            </Gmaps>
        );
    }

});

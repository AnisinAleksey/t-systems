import '../css/404.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );

export var ErrorForm = React.createClass({
    render: function() {
        return (
            <div className="errorForm">
                <form>
                    <span className="has-error">404</span>
                    <br/>
                    <span className="textError">Page Not Found</span>
                    <br/>
                </form>
            </div>
        );
    }
});
ReactDOM.render(<ErrorForm/>,document.body);
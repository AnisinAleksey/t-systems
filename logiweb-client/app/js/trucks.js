import '../css/trucks.css';
import '../css/breadcrumbs.css';
import '../css/bootstrap.min.css';
import '../css/react-bootstrap-table-all.min.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );
global.jQuery =  $;
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal';
import Alert from 'react-bootstrap/lib/Alert';
import {BreadCrumbsComponent} from './breadcrumbs';
import {DataListCities} from './dataListCities';
require( 'bootstrap' );

export var TrucksDataTable = React.createClass({
    getInitialState() {
        return {
            data: [],
            showModal: false,
            cityList: []
        };
    },
    componentWillMount: function(){
        var trucksData = [{
            vin: 15,
            driversCount: 2,
            capacity: 100,
            state: "Ok",
            city: "Berlin",
        },{
            vin: 555,
            driversCount: 6,
            capacity: 100,
            state: "Ok",
            city: "Bonn",
        },
            {
                vin: 55533,
                driversCount: 5,
                capacity: 10,
                state: "Bad",
                city: "Drezden",
            }];
        var trucksData = [];
        $.ajax({
            url: "/employee/trucks",
            async: false,
            method: 'GET',
            success: function(data) {
                trucksData = JSON.parse(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                    messageText="Unable get trucks"/>, document.getElementById('messageAlertDiv'));
            }
        });
        this.state.data = trucksData;
    },
    setSelectRow : function(row){
        //this.state.row = row;
    },

    addTruck : function(row){
        var result = false;
        $.ajax({
            contentType: "application/json",
            url: "/employee/trucks",
            async: false,
            method: 'POST',
            data: JSON.stringify(row),
            success: function() {
                ReactDOM.render(<MessageAlert type="success"
                                              messageText="Truck was created"/>, document.getElementById('messageAlertDiv'));
                result = true;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable create truck"/>, document.getElementById('messageAlertDiv'));
                result = false;
            }
        });
        if(result) {
            var res = this.state.data;
            res.push(row);
            this.setState({
                data: res
            });
        }
    },

    deleteTruck : function(row){
        this.closeConfirm();
        var isRemove = true;
        $.ajax({
            url: "/employee/trucks/" + row,
            async: false,
            method: 'DELETE',
            success: function(data) {
                ReactDOM.render(<MessageAlert type="success"
                                              messageText="Truck was deleted"/>, document.getElementById('messageAlertDiv'));
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable delete truck"/>, document.getElementById('messageAlertDiv'));
                isRemove=false;
            }
        });
        if(isRemove) {
            var trucks = this.state.data.filter((truck) => {
                return truck.regnumber !== row[0];
            });

            this.setState({
                data: trucks
            });
        }
    },
    handleDeleteRow: function (next, rowKeys){
        this.openConfirm(next, rowKeys);
    },
    closeConfirm() {
        this.setState({ showModal: false });
    },
    openConfirm(next, rowKeys) {
        this.setState({ showModal: true}, function(){
            $("#openConfirmButton").click(function(){
                next.call();
            });
        });
    },
    onBeforeSaveCell : function(row, cellName, cellValue){
        var result = false;
        row[cellName] = cellValue;
        $.ajax({
            contentType: "application/json",
            url: "/employee/trucks",
            async: false,
            method: 'PUT',
            data: JSON.stringify(row),
            success: function(jqXHR, textStatus) {
                ReactDOM.render(<MessageAlert type="success"
                                              messageText="Truck was updated"/>, document.getElementById('messageAlertDiv'));
                result = true;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable update truck"/>, document.getElementById('messageAlertDiv'));
                result = false;
            }
        });
        return result;
    },
    onAfterSaveCell : function(row, cellName, cellValue){
        console.log("Save cell '"+cellName+"' with value '"+cellValue+"'");
        console.log("Thw whole row :");
        console.log(row);
    },
    numberValidator : function(value){
        if(!value){
            return 'Not empty field'
        }else if(!(/^\d+$/.test(value))){
            return 'Only numbers!'
        }
        return true;
    },
    textValidator : function(value){
        if(!value){
            return 'Not empty field'
        }else if(!(/^[a-zA-Z]+$/.test(value))){
            return 'Only characters!'
        }
        return true;
    },
    vinValidator : function(value){
        if(!value){
            return 'Not empty field'
        }else if(!(/^[a-zA-Z]{2}\d{5}$/.test(value))){
            return 'Uncorrect format of vin!'
        }
        return true;
    },
    render: function(){
        $( document ).on( "focus", ".form-control.editor.edit-text", function(event) {
            var label = event.target.previousSibling;
            if(label != undefined && label.textContent == "City") {
                event.target.setAttribute("list", "fillCity");
            }
        }).on("keyup", ".form-control.editor.edit-text", function(event) {
            var label = event.target.previousSibling;
            if(label != undefined && label.textContent == "City"){
                $.ajax({
                    url: "/employee/cities/search",
                    async: false,
                    method: 'GET',
                    data: {phrase : event.target.value},
                    success: function(data) {
                        ReactDOM.render(<DataListCities idList="fillCity" cityList={JSON.parse(data)}/>, document.getElementById('dataList'));
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    }
                });
            }
        });
        var selectRowProp = {
            mode: "radio",
            clickToSelect: true,
            bgColor: "rgb(238, 193, 213)",
            onSelect: this.setSelectRow
        };
        var options = {
            onAddRow : this.addTruck,
            onDeleteRow: this.deleteTruck,
            handleConfirmDeleteRow : this.handleDeleteRow,
            ignoreEditable : true
        };
        var cellEditProp = {
            mode: "click",
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell
        };
        return (
            <div>
                <div id="dataList"></div>
                <datalist id="fillCity">
                    {this.state.cityList}
                </datalist>
                <div id = "messageAlertDiv"></div>
                <div>
                    <BootstrapTable remote={true} search={true} data={this.state.data} striped={true} hover={true} pagination = {true} selectRow={selectRowProp} insertRow={true} deleteRow={true} options ={options} cellEdit={cellEditProp} id = "truckDataTable">
                        <TableHeaderColumn dataField="regnumber" isKey={true} dataAlign="center" dataSort={true} editable={{type:'text', validator: this.vinValidator}}>VIN</TableHeaderColumn>
                        <TableHeaderColumn dataField="count_drivers" dataSort={true} editable={{type:'text', validator: this.numberValidator}}>Count</TableHeaderColumn>
                        <TableHeaderColumn dataField="capacity" dataSort={true} editable={{type:'text', validator: this.numberValidator}}>Capacity</TableHeaderColumn>
                        <TableHeaderColumn dataField="state" dataSort={true} editable={{type:'select', options:{values:["OK", "BAD"]}}}>State</TableHeaderColumn>
                        <TableHeaderColumn editable={false} dataField="city" dataSort={true}>City</TableHeaderColumn>
                    </BootstrapTable>
                    <Modal show={this.state.showModal} onHide={this.closeConfirm} id="confirmDeletingDiv">
                        <Modal.Header closeButton>
                            <Modal.Title>Are you sure?</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Would you like to remove this truck?
                        </Modal.Body>
                        <Modal.Footer>
                            <Button
                                bsStyle="success"
                                bsSize="sm"
                                id="openConfirmButton">
                                Yes
                            </Button>
                            <Button
                                bsStyle="danger"
                                bsSize="sm"
                                onClick={this.closeConfirm}>
                                No
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        )
    }
});


const MessageAlert = React.createClass({
    propTypes: {
        messageText: React.PropTypes.string.isRequired,
        type: React.PropTypes.string.isRequired
    },
    render() {
        this.handleAlertShow();
        return (
            <Alert id= "alarmingDiv" bsStyle={this.props.type} onDismiss={this.handleAlertDismiss}>
                <p>{this.props.messageText}</p>
            </Alert>
        );
    },
    handleAlertDismiss : function() {
        $("#alarmingDiv").hide();
    },

    handleAlertShow() {
        $("#alarmingDiv").show();
    }
});

ReactDOM.render(<BreadCrumbsComponent activeTab='trucks'/>, document.getElementById("breadcrumbsDiv"));
ReactDOM.render(<TrucksDataTable/>, document.getElementById("truckDiv"));
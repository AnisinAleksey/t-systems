import '../css/login.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );

export var LoginForm = React.createClass({
    getInitialState: function(){
        return {
            username: '',
            password: ''
        }
    },
    login : function(e) {
        e.preventDefault();
        $.ajax({
            url: "/login",
            async: false,
            method: "POST",
            data: {username: this.state.username, password: this.state.password},
            success: function() {
                window.location.href = "/";
            },
            statusCode: {
                403: function(data) {
                    LoginForm.prototype.showInvisibleSpan(data.statusText);
                }
            }
        })
    },
    hideInvisibleSpan : function(){
        $("#hiddenSpan").css("visibility", "hidden");
    },
    handleChangeUsername : function(e){
        this.hideInvisibleSpan();
        this.setState({username: e.target.value}, function(){
            this.checkUserNameAndPassword();
        });
    },
    checkUserNameAndPassword : function(){
        if(this.state.username == "" && this.state.password == ""){
            $("#loginButton").prop( "disabled", true );
            this.showInvisibleSpan("Please, enter username and password");
        }
        else if(this.state.username == ""){
            $("#loginButton").prop( "disabled", true );
            this.showInvisibleSpan("Please, enter username");
        }
        else if (this.state.password == ""){
            $("#loginButton").prop( "disabled", true );
            this.showInvisibleSpan("Please, enter password");
        }
        else{
            $("#loginButton").prop( "disabled", false);
        }
    },
    showInvisibleSpan: function(text){
        $("#hiddenSpan").text(text);
        $("#hiddenSpan").css("visibility", "visible");
    },
    handleChangePassword: function(e){
        this.hideInvisibleSpan();
        this.setState({password: e.target.value}, function(){
            this.checkUserNameAndPassword();
        });
    },
    render: function() {
        return (
            <div className = "loginForm">
                <form autocomplete="off" onSubmit={this.login}>
                    <span className = "headerSpan">Sign up</span>
                    <br/>
                    <span id = "hiddenSpan" className = "hiddenSpan">Incorrect login or password</span>
                    <div className="fieldDiv">
                        <input id="login" type="text" className="loginField" value="" placeholder="username" value = {this.state.username} onChange={this.handleChangeUsername} name= "username"/>
                    </div>
                    <div className="fieldDiv">
                        <input id="password" type="password" className="loginField" placeholder="password" onChange={this.handleChangePassword} name= "password"/>
                    </div>
                    <button type="submit" className="loginButtonClass" id ="loginButton" disabled="true">Sign up</button>
                </form>
            </div>
        );
    }
});
ReactDOM.render(<LoginForm/>,document.body);

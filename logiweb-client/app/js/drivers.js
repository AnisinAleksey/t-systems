import '../css/drivers.css';
import '../css/breadcrumbs.css';
import '../css/bootstrap.min.css';
import '../css/react-bootstrap-table-all.min.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );
global.jQuery =  $;
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Button from 'react-bootstrap/lib/Button';
import Modal from 'react-bootstrap/lib/Modal';
import Alert from 'react-bootstrap/lib/Alert';
import {BreadCrumbsComponent} from './breadcrumbs';
import {DataListCities} from './dataListCities';
require( 'bootstrap' );

export var DriversDataTable = React.createClass({
    getInitialState() {
        return {
            data: [],
            showModal: false,
            cityList: []
        };
    },
    componentWillMount: function(){
        var driversData = [];
        $.ajax({
            url: "/employee/drivers",
            async: false,
            method: 'GET',
            success: function(data) {
                driversData = JSON.parse(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable get drivers"/>, document.getElementById('messageAlertDiv'));
            }
        });
        this.state.data = driversData;
    },

    setSelectRow : function(row){
        //this.state.row = row;
    },
    addDriver : function(row){
        var result = false;
        $.ajax({
            contentType: "application/json",
            url: "/employee/drivers",
            async: false,
            method: 'POST',
            data: JSON.stringify(row),
            success: function(data) {
                var Message = "Driver was created. Password: '" + data + "' was generated automatically. Please remember and provide it to Driver!!! ";
                ReactDOM.render(<MessageAlert type="success"
                                              messageText={Message}/>, document.getElementById('messageAlertDiv'));
                result = true;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable create driver"/>, document.getElementById('messageAlertDiv'));
                result = false;
            }
        });
        if(result) {
            var res = this.state.data;
            res.push(row);
            this.setState({
                data: res
            });
        }
    },
    deleteDriver : function(row){
        this.closeConfirm();
        var isRemove = true;
        $.ajax({
            url: "/employee/drivers/" + row,
            async: false,
            method: 'DELETE',
            success: function(data) {
                ReactDOM.render(<MessageAlert type="success"
                                              messageText="Driver was deleted"/>, document.getElementById('messageAlertDiv'));
            },
            error: function(jqXHR, textStatus, errorThrown) {
                isRemove=false;
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable delete driver"/>, document.getElementById('messageAlertDiv'));
            }
        });
        if(isRemove) {
            var drivers = this.state.data.filter((driver) => {
                return driver.p_number !== row[0];
            });

            this.setState({
                data: drivers
            });
        }
    },
    handleDeleteRow: function (next, rowKeys){
        this.openConfirm(next, rowKeys);
    },
    closeConfirm() {
        this.setState({ showModal: false });
    },
    openConfirm(next, rowKeys) {
        this.setState({ showModal: true}, function(){
            $("#openConfirmButton").click(function(){
                next.call();
            });
        });
    },

    beforeEditCell : function(row, cellName, cellValue){
        var result = false;
        row[cellName] = cellValue;
        $.ajax({
            contentType: "applicat ion/json",
            url: "/employee/drivers",
            async: false,
            method: 'PUT',
            data: JSON.stringify(row),
            success: function() {
                ReactDOM.render(<MessageAlert type="success"
                                              messageText="Driver was updated"/>, document.getElementById('messageAlertDiv'));
                result = true;
            },
            error: function(jqXHR, textStatus, errorThrown) {
                ReactDOM.render(<MessageAlert type="danger"
                                              messageText="Unable update driver"/>, document.getElementById('messageAlertDiv'));
                result = false;
            }
        });
        return result;
    },
    onAfterSaveCell : function(row, cellName, cellValue){
        console.log("Save cell '"+cellName+"' with value '"+cellValue+"'");
        console.log("Thw whole row :");
        console.log(row);
    },
    numberValidator : function(value){
        if(!value){
            return 'Not empty field'
        }else if(!(/^\d+$/.test(value))){
            return 'Only numbers!'
        }
        return true;
    },
    textValidator : function(value){
        if(!value){
            return 'Not empty field'
        }else if(!(/^[a-zA-Z]+$/.test(value))){
            return 'Only characters!'
        }
        return true;
    },
    render: function(){
        $( document ).on( "focus", ".form-control.editor.edit-text", function(event) {
            var label = event.target.previousSibling;
            if(label != undefined && label.textContent == "City") {
                event.target.setAttribute("list", "fillCity");
            }
        }).on("keyup", ".form-control.editor.edit-text", function(event) {
            var label = event.target.previousSibling;
            if(label != undefined && label.textContent == "City"){
                $.ajax({
                    url: "/employee/cities/search",
                    async: false,
                    method: 'GET',
                    data: {phrase : event.target.value},
                    success: function(data) {
                        ReactDOM.render(<DataListCities idList="fillCity" cityList={JSON.parse(data)}/>, document.getElementById('dataList'));
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    }
                });
            }
        });
        var selectRowProp = {
            mode: "radio",
            clickToSelect: true,
            bgColor: "rgb(238, 193, 213)",
            onSelect: this.setSelectRow
        };
        var options = {
            onAddRow : this.addDriver,
            onDeleteRow: this.deleteDriver,
            handleConfirmDeleteRow : this.handleDeleteRow,
            ignoreEditable : true
        };
        var cellEditProp = {
            mode: "click",
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.beforeEditCell
        }
        return (
            <div>
                <div id="dataList"></div>
                <div id = "messageAlertDiv"></div>
                <div>
                    <BootstrapTable remore={true} data={this.state.data} striped={true} hover={true} pagination = {true} selectRow={selectRowProp} insertRow={true} deleteRow={true} options ={options} cellEdit={cellEditProp} id = "driverDataTable">
                        <TableHeaderColumn dataField="name" dataSort={true} editable={{type:'text', validator: this.textValidator}}>Name</TableHeaderColumn>
                        <TableHeaderColumn dataField="surname" dataSort={true} editable={{type:'text', validator: this.textValidator}}>Surname</TableHeaderColumn>
                        <TableHeaderColumn dataField="p_number" isKey={true} dataAlign="center" dataSort={true}>Personal Number</TableHeaderColumn>
                        <TableHeaderColumn dataField="status" dataSort={true} editable={{type:'select', options:{values:["REST", "BUSY", "DRIVING"]}}}>Status</TableHeaderColumn>
                        <TableHeaderColumn dataField="city" dataSort={true} editable={false}>City</TableHeaderColumn>
                    </BootstrapTable>
                    <Modal show={this.state.showModal} onHide={this.closeConfirm} id="confirmDeletingDiv">
                        <Modal.Header closeButton>
                            <Modal.Title>Are you sure?</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Would you like to remove this driver?
                        </Modal.Body>
                        <Modal.Footer>
                            <Button
                                bsStyle="success"
                                bsSize="sm"
                                id="openConfirmButton">
                                Yes
                            </Button>
                            <Button
                                bsStyle="danger"
                                bsSize="sm"
                                onClick={this.closeConfirm}>
                                No
                            </Button>
                        </Modal.Footer>
                    </Modal>
                </div>
            </div>
        )
    }
});
const MessageAlert = React.createClass({
    propTypes: {
        messageText: React.PropTypes.string.isRequired,
        type: React.PropTypes.string.isRequired
    },
    render() {
        this.handleAlertShow();
        return (
            <Alert id= "alarmingDiv" bsStyle={this.props.type} onDismiss={this.handleAlertDismiss}>
                <p>{this.props.messageText}</p>
            </Alert>
        );
    },
    handleAlertDismiss : function() {
        $("#alarmingDiv").hide();
    },

    handleAlertShow() {
        $("#alarmingDiv").show();
    }
});

ReactDOM.render(<BreadCrumbsComponent activeTab='drivers'/>, document.getElementById("breadcrumbsDiv"));
ReactDOM.render(<DriversDataTable/>, document.getElementById("driverDiv"));
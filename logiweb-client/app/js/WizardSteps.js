import '../css/orders.css';
import '../css/breadcrumbs.css';
import '../css/bootstrap.min.css';
import '../css/react-bootstrap-table-all.min.css';
import 'react-select/dist/react-select.css';

var React = require('react');
var ReactDOM = require('react-dom');
var Wizard = require('react-wizard');
var $ = require('jquery');
var Select = require('react-select');
global.jQuery = $;
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Button from 'react-bootstrap/lib/Button';
import {DataListCities} from './dataListCities';
import {RouteTable} from './routeTable';
import {CargoesTable} from './cargoesTable';
import {SelectTruckTable} from './selectTruckTable';
import {SelectDriverTable} from './selectDriverTable';
require('bootstrap');
require('jquery.cookie');

export var RouteStep = {
    name: 'Route',
    onSubmit: function (done) {
        if ($.cookie("route") != undefined) {
            var routeArray = JSON.parse($.cookie("route"));
            if (routeArray.length < 2) {
                return done(new Error('You need choose minimum two city'));
            }
            done();
        }
        else {
            return done(new Error('You need choose minimum two city'));
        }
    },
    component: React.createClass({
        displayName: 'component',
        getInitialState() {
            return {
                route: [],
                city: ""
            };
        },
        componentWillMount: function () {
            if ($.cookie("route") != undefined) {
                var routeArray = JSON.parse($.cookie("route"));
                if (routeArray.length != undefined && routeArray.length != 0) {
                    this.setState({route: routeArray}, function () {
                        ReactDOM.render(<RouteTable
                            cityData={this.state.route}/>, document.getElementById("routeTableDiv"));
                    });
                }
            }
        },
        autoCompleteCity: function (event) {
            $.ajax({
                url: "/employee/cities/search",
                async: false,
                method: 'GET',
                data: {phrase: event.target.value},
                success: function (data) {
                    ReactDOM.render(<DataListCities idList="fillCity"
                                                    cityList={JSON.parse(data)}/>, document.getElementById('dataList'));
                },
                error: function (jqXHR, textStatus, errorThrown) {
                }
            });
            this.setState({city: event.target.value});
        },
        saveCityInRoute: function () {
            if (!(/^[a-zA-Z]+$/.test(this.state.city))) {
                alert("Not empty city");
            }
            else {
                function RouteElement(city, originalNum) {
                    this.city = city;
                    this.originalNum = originalNum;
                }

                RouteElement.prototype.toString = function () {
                    return this.city;
                };

                var routeArray = this.state.route;
                var position = routeArray.length;
                var isCity = false;
                var cityName = this.state.city;
                $.grep(routeArray, function (item, i) {
                    if (item.city == cityName) {
                        isCity = true;
                    }
                });
                if (!isCity) {
                    routeArray.push(new RouteElement(this.state.city, position));
                    $.cookie('route', JSON.stringify(routeArray));
                    this.setState({route: routeArray}, function () {
                        ReactDOM.render(<RouteTable
                            cityData={this.state.route}/>, document.getElementById("routeTableDiv"));
                    })
                }
                else{
                    alert("Unable add city");
                }
            }
        },
        render: function () {
            return (
                <div>
                    <label className="wizardHeaderLabel">Please, enter the city in correct order</label>
                    <br/>
                    <input type="text" list="fillCity" id="cityInRoute" onChange={this.autoCompleteCity}/>
                    <div id="dataList"></div>
                    <Button
                        bsStyle="success"
                        bsSize="sm"
                        onClick={this.saveCityInRoute}
                        id="saveCityButton">
                        Add
                    </Button>
                    <div id="routeTableDiv"></div>
                </div>
            );
        }
    })
};

export var CargoStep = {
    name: 'Cargo',
    onSubmit: function (done) {
        if ($.cookie("cargoes") != undefined) {
            var cargoesArray = JSON.parse($.cookie("cargoes"));
            if (cargoesArray.length < 1) {
                return done(new Error('You need create minimum one cargo'));
            }
            done();
        }
        else {
            return done(new Error('You need create minimum one cargo'));
        }
    },
    component: React.createClass({
        displayName: 'component',
        getInitialState() {
            return {
                optionsLoad: [],
                optionsUnload: [],
                cargoName: "",
                weight: "",
                load: "",
                unload: "",
                routeCities: [],
                cargoes: []
            };
        },
        componentWillMount: function () {
            var cargoesArray = [];
            var cities = [];

            function Option(city) {
                this.value = city;
                this.label = city;
            }

            var routeArray = JSON.parse($.cookie("route"));
            routeArray.forEach(function (item, i, routeArray) {
                cities.push(new Option(item.city))
            });
            if ($.cookie("cargoes") != undefined) {
                cargoesArray = JSON.parse($.cookie("cargoes"));
            }
            this.setState({
                optionsLoad: cities,
                optionsUnload: cities,
                routeCities: cities,
                cargoes: cargoesArray
            }, function () {
                if (this.state.cargoes != [] && this.state.cargoes.length != 0) {
                    ReactDOM.render(<CargoesTable
                        cargoes={this.state.cargoes}/>, document.getElementById("cargoesTableDiv"));
                }
            })
        },
        componentDidMount: function () {
            $("#saveCargoButton").prop("disabled", true);
        },
        handleChangeCargoName: function (e) {
            this.setState({cargoName: e.target.value}, function () {
                this.checkCargo();
            });
        },
        handleChangeCargoWeight: function (e) {
            this.setState({weight: e.target.value}, function () {
                this.checkCargo();
            });
        },
        handleChangeLoadCity: function (e) {
            function Option(city) {
                this.value = city;
                this.label = city;
            }

            var indexLoad = 0;
            var unloadArray = JSON.parse($.cookie("route"));
            $.grep(unloadArray, function (item, i) {
                if (item.city == e.value) {
                    indexLoad = item.originalNum;
                }
            });
            var unloadArrayOptions = [];
            $.grep(unloadArray, function (item, i) {
                if (item.originalNum > indexLoad) {
                    unloadArrayOptions.push(new Option(item.city))
                }
            });
            this.setState({
                load: e.value,
                optionsUnload: unloadArrayOptions
            }, function () {
                this.checkCargo();
            });
        },
        handleChangeUnloadCity: function (e) {
            function Option(city) {
                this.value = city;
                this.label = city;
            }

            var loadArray = JSON.parse($.cookie("route"));
            var indexLoad = loadArray.length;
            $.grep(loadArray, function (item, i) {
                if (item.city == e.value) {
                    indexLoad = item.originalNum;
                }
            });
            var loadArrayOptions = [];
            $.grep(loadArray, function (item, i) {
                if (item.originalNum < indexLoad) {
                    loadArrayOptions.push(new Option(item.city))
                }
            });
            this.setState({
                unload: e.value,
                optionsLoad: loadArrayOptions
            }, function () {
                this.checkCargo();
            });
        },
        checkCargo: function () {
            if (this.state.cargoName != "" && (/^\d+$/.test(this.state.weight)) && this.state.load != "" && this.state.unload != "") {
                $("#saveCargoButton").prop("disabled", false);
            }
            else {
                $("#saveCargoButton").prop("disabled", true);
            }
        },
        saveCargo: function () {
            var cargoArray = this.state.cargoes;

            function CargoObject(name, weight, load, unload) {
                this.name = name;
                this.weight = weight;
                this.load = load;
                this.unload = unload
            }

            var routeArray = JSON.parse($.cookie("route"));
            var load = this.state.load;
            load = routeArray.filter((element) => {
                return element.city == load;
            });
            load = load.length == 0 ? "" : load[0];
            var unload = this.state.unload;
            var unload = routeArray.filter((element) => {
                return element.city == unload;
            });
            unload = unload.length == 0 ? "" : unload[0];
            cargoArray.push(
                new CargoObject(this.state.cargoName, this.state.weight, load, unload)
            );
            var citiesArray = this.state.routeCities;
            this.setState({
                cargoes: cargoArray,
                cargoName: "",
                weight: "",
                load: "",
                unload: "",
                optionsLoad: citiesArray,
                optionsUnload: citiesArray,
            }, function () {
                $.cookie('cargoes', JSON.stringify(cargoArray));
                ReactDOM.render(<CargoesTable
                    cargoes={this.state.cargoes}/>, document.getElementById("cargoesTableDiv"));
            });
            $("#saveCargoButton").prop("disabled", true);

        },
        render: function () {
            return (
                <div>
                    <label className="wizardHeaderLabel">Please, create cargo</label>
                    <br/>
                    <label className="cargoLabel">Cargo name</label>
                    <input className="cargoField" type="text" placeholder="Cargo name" value={this.state.cargoName}
                           onChange={this.handleChangeCargoName}/>
                    <br/>
                    <label className="cargoLabel">Weight</label>
                    <input className="cargoField" type="text" placeholder="Weight" value={this.state.weight}
                           onChange={this.handleChangeCargoWeight}/>
                    <br/>
                    <label className="cargoLabel">Load</label>
                    <div id="loadDiv">
                        <Select
                            name="form-field-name"
                            value={this.state.load}
                            options={this.state.optionsLoad}
                            className="cargoField"
                            onChange={this.handleChangeLoadCity}
                        />
                    </div>
                    <br/>
                    <label className="cargoLabel">Unload</label>
                    <div id="unloadDiv">
                        <Select
                            name="form-field-name"
                            value={this.state.unload}
                            options={this.state.optionsUnload}
                            className="cargoField"
                            onChange={this.handleChangeUnloadCity}
                        />
                    </div>
                    <br/>

                    <Button
                        bsStyle="success"
                        bsSize="sm"
                        onClick={this.saveCargo}
                        id="saveCargoButton">
                        Save
                    </Button>
                    <div id="cargoesTableDiv"></div>
                </div>
            );
        }
    })
};

export var TruckStep = {
    name: 'Truck',
    onSubmit: function (done) {
        if ($.cookie("truck") != undefined) {
            var truckArray = JSON.parse($.cookie("truck"));
            if (truckArray.length == 0) {
                return done(new Error('You need choose truck'));
            }
            done();
        }
        else {
            return done(new Error('You need choose truck'));
        }
    },
    component: React.createClass({
        displayName: 'component',
        getInitialState() {
            return {
                truckData: [],
                selectedCar: []
            };
        },
        componentWillMount: function () {
            var resultTrucks = [];
            var selectedTruck = "";
            var cargoes = JSON.parse($.cookie("cargoes"));
            $.ajax({
                contentType: "application/json",
                url: "/employee/orders/availableTrucks",
                async: false,
                method: 'POST',
                data: JSON.stringify(cargoes),
                success: function (data) {
                    resultTrucks = JSON.parse(data);
                },
                error: function () {
                    alert("error");
                }
            });
            //resultTrucks = [{
            //    vin: 15,
            //    driversCount: 2,
            //    capacity: 100,
            //    state: "Ok",
            //    city: "Berlin",
            //},{
            //    vin: 555,
            //    driversCount: 6,
            //    capacity: 100,
            //    state: "Ok",
            //    city: "Bonn",
            //},
            //    {
            //        vin: 55533,
            //        driversCount: 5,
            //        capacity: 10,
            //        state: "Bad",
            //        city: "Drezden",
            //    }];
            if ($.cookie('truck') != undefined) {
                selectedTruck = JSON.parse($.cookie('truck'));
            }
            this.setState({
                truckData: resultTrucks,
                selectedCar: selectedTruck
            }, function () {
                if (this.state.selectedCar != []) {
                    ReactDOM.render(<SelectTruckTable
                        truck={this.state.selectedCar}/>, document.getElementById("selectTruckTable"));
                }
            });
        },
        setSelectRow: function (row) {
            var rowArray = [];
            rowArray.push(row);
            this.setState({
                selectedCar: rowArray
            }, function () {
                ReactDOM.render(<SelectTruckTable
                    truck={this.state.selectedCar}/>, document.getElementById("selectTruckTable"));
                $.cookie('truck', JSON.stringify(this.state.selectedCar));
            });
        },
        render: function () {
            var selectRowProp = {
                mode: "radio",
                clickToSelect: true,
                bgColor: "rgb(238, 193, 213)",
                onSelect: this.setSelectRow
            };
            return (
                <div>
                    <label className="wizardHeaderLabel">Choose truck from the available</label>
                    <BootstrapTable search={true} data={this.state.truckData} striped={true} hover={true}
                                    selectRow={selectRowProp}>
                        <TableHeaderColumn dataField="regnumber" isKey={true} dataAlign="center"
                                           dataSort={true}>VIN</TableHeaderColumn>
                        <TableHeaderColumn dataField="count_drivers" dataSort={true}>Count</TableHeaderColumn>
                        <TableHeaderColumn dataField="capacity" dataSort={true}>Capacity</TableHeaderColumn>
                        <TableHeaderColumn dataField="state" dataSort={true}>State</TableHeaderColumn>
                        <TableHeaderColumn dataField="city" dataSort={true}>City</TableHeaderColumn>
                    </BootstrapTable>
                    <div id="selectTruckTable"></div>
                </div>
            );
        }
    })
};

export var DriverStep = {
    name: 'Driver',
    onSubmit: function (done) {
        if ($.cookie("drivers") != undefined) {
            var cargoesArray = JSON.parse($.cookie("drivers"));
            var truckArray = JSON.parse($.cookie("truck"));
            var driversCount = truckArray[0].count_drivers;
            if (cargoesArray.length != driversCount) {
                return done(new Error('You need add ' + driversCount + ' drivers'));
            }
            done();
        }
        else {
            return done(new Error('You need add ' + this.state.countDrivers + ' drivers'));
        }
    },
    component: React.createClass({
        displayName: 'component',
        getInitialState() {
            return {
                countDrivers: 0,
                driversArray: [],
                selectedDriversArray: [],
                selectedDriver: ""
            };
        },
        componentWillMount: function () {
            var resultDrivers = [];
            var selectedDrivers = [];

            function OrderObject(truck, waypoints) {
                this.truck = truck;
                this.waypoints = waypoints;
            }

            var truckArray = JSON.parse($.cookie("truck"));
            var driversCount = truckArray[0].driversCount;
            var order = new OrderObject(truckArray[0].regnumber, JSON.parse($.cookie("route")));
            $.ajax({
                contentType: "application/json",
                url: "/employee/orders/availableDrivers",
                async: false,
                method: 'POST',
                data: JSON.stringify(order),
                success: function (data) {
                    resultDrivers = JSON.parse(data);
                }
            });
            if ($.cookie('drivers') != undefined) {
                selectedDrivers = JSON.parse($.cookie('drivers'));
                selectedDrivers.forEach(function (item, i, selectedDrivers) {
                    resultDrivers.splice($.inArray(item, resultDrivers), 1);
                });
            }
            this.setState({
                driversArray: resultDrivers,
                selectedDriversArray: selectedDrivers,
                countDrivers: driversCount
            }, function () {
                if (this.state.selectedDriversArray != [] && this.state.selectedDriversArray.length != 0) {
                    ReactDOM.render(<SelectDriverTable
                        drivers={this.state.selectedDriversArray}/>, document.getElementById("selectDriverTable"));
                }
            });
        },
        setSelectRow: function (row) {
            this.setState({
                selectedDriver: row
            })
        },
        saveDriver: function () {
            var driver = this.state.selectedDriver;
            var drivers = this.state.driversArray;
            var rowArray = this.state.selectedDriversArray;
            rowArray.push(driver);
            function DriverObject(name, surname, p_number, status, city) {
                this.name = name;
                this.surname = surname;
                this.p_number = p_number;
                this.status = status;
                this.city = city;
            }

            var itemToRemove = new DriverObject(driver.name, driver.surname, driver.p_number, driver.status, driver.city);
            drivers = $.grep(drivers, function (item, i) {
                if (!(item.name == itemToRemove.name && item.surname == itemToRemove.surname && item.p_number == itemToRemove.p_number
                    && item.status == itemToRemove.status && item.city == itemToRemove.city)) {
                    return item;
                }
            });
            this.setState({
                selectedDriversArray: rowArray,
                driversArray: drivers
            }, function () {
                ReactDOM.render(<SelectDriverTable
                    drivers={this.state.selectedDriversArray}/>, document.getElementById("selectDriverTable"));
                $.cookie('drivers', JSON.stringify(this.state.selectedDriversArray));
            });
        },
        clearDriver: function () {
            var drivers = this.state.driversArray;
            var selectedDrivers = this.state.selectedDriversArray;
            selectedDrivers.forEach(function (item, i, selectedDrivers) {
                drivers.push(item);
            });
            this.setState({
                selectedDriversArray: [],
                driversArray: drivers
            }, function () {
                ReactDOM.render(<SelectDriverTable
                    drivers={this.state.selectedDriversArray}/>, document.getElementById("selectDriverTable"));
                $.removeCookie('drivers');
            });
        },
        render: function () {
            var selectRowProp = {
                mode: "radio",
                clickToSelect: true,
                bgColor: "rgb(238, 193, 213)",
                onSelect: this.setSelectRow
            };
            return (
                <div>
                    <label className="wizardHeaderLabel">Choose {this.state.countDrivers} drivers from the
                        available</label>
                    <BootstrapTable data={this.state.driversArray} striped={true} hover={true}
                                    selectRow={selectRowProp}>
                        <TableHeaderColumn dataField="name" dataSort={true}>Name</TableHeaderColumn>
                        <TableHeaderColumn dataField="surname" dataSort={true}>Surname</TableHeaderColumn>
                        <TableHeaderColumn dataField="p_number" isKey={true} dataAlign="center" dataSort={true}>Personal
                            Number</TableHeaderColumn>
                        <TableHeaderColumn dataField="status" dataSort={true}>Status</TableHeaderColumn>
                        <TableHeaderColumn dataField="city" dataSort={true}>City</TableHeaderColumn>
                    </BootstrapTable>
                    <Button
                        bsStyle="success"
                        bsSize="sm"
                        onClick={this.saveDriver}
                        id="saveDriverButton">
                        Choose
                    </Button>
                    <Button
                        bsStyle="danger"
                        bsSize="sm"
                        onClick={this.clearDriver}
                        id="cleadSelectedDriverButton">
                        Clear
                    </Button>
                    <div id="selectDriverTable"></div>
                </div>
            );
        }
    })
};

export var OrderStep = {
    name: 'Order',
    onSubmit: function (done) {
        done();
    },
    component: React.createClass({
        displayName: 'component',
        getInitialState() {
            return {
                route: [],
                cargoesData: [],
                selectedTruck: [],
                driversData: []
            };
        },
        componentWillMount: function () {
            var routeArray = [];
            var cargoesArray = [];
            var selectedCar = [];
            var driversArray = [];
            if ($.cookie("route") != undefined) {
                routeArray = JSON.parse($.cookie("route"));
            }
            if ($.cookie("cargoes") != undefined) {
                cargoesArray = JSON.parse($.cookie("cargoes"));
                cargoesArray.forEach((cargo) => {
                    cargo.load = cargo.load.city;
                    cargo.unload = cargo.unload.city;
                });
            }
            if ($.cookie("truck") != undefined) {
                selectedCar = JSON.parse($.cookie('truck'));
            }
            if ($.cookie("drivers") != undefined) {
                driversArray = JSON.parse($.cookie('drivers'));
            }
            this.setState({
                route: routeArray,
                cargoesData: cargoesArray,
                selectedTruck: selectedCar,
                driversData: driversArray
            });
        },
        render: function () {
            return (
                <div>
                    <label className="wizardHeaderLabel">Check your order</label>
                    <br/>
                    <label className="wizardHeaderLabel">Route</label>
                    <BootstrapTable data={this.state.route} striped={true}>
                        <TableHeaderColumn dataField="city" isKey={true} dataAlign="center">City</TableHeaderColumn>
                    </BootstrapTable>
                    <label className="wizardHeaderLabel">Orders</label>
                    <BootstrapTable data={this.state.cargoesData} striped={true}>
                        <TableHeaderColumn dataField="name" isKey={true} dataAlign="center">Name</TableHeaderColumn>
                        <TableHeaderColumn dataField="weight" dataAlign="center">Weight</TableHeaderColumn>
                        <TableHeaderColumn dataField="load" dataAlign="center">Load</TableHeaderColumn>
                        <TableHeaderColumn dataField="unload" dataAlign="center">Unload</TableHeaderColumn>
                    </BootstrapTable>
                    <label className="wizardHeaderLabel">Truck</label>
                    <BootstrapTable data={this.state.selectedTruck} striped={true}>
                        <TableHeaderColumn dataField="regnumber" isKey={true} dataAlign="center"
                                           dataSort={true}>VIN</TableHeaderColumn>
                        <TableHeaderColumn dataField="count_drivers" dataSort={true}>Count</TableHeaderColumn>
                        <TableHeaderColumn dataField="capacity" dataSort={true}>Capacity</TableHeaderColumn>
                        <TableHeaderColumn dataField="state" dataSort={true}>State</TableHeaderColumn>
                        <TableHeaderColumn dataField="city" dataSort={true}>City</TableHeaderColumn>
                    </BootstrapTable>
                    <label className="wizardHeaderLabel">Drivers</label>
                    <BootstrapTable data={this.state.driversData} striped={true}>
                        <TableHeaderColumn dataField="name" dataSort={true}>Name</TableHeaderColumn>
                        <TableHeaderColumn dataField="surname" dataSort={true}>Surname</TableHeaderColumn>
                        <TableHeaderColumn dataField="p_number" isKey={true} dataAlign="center" dataSort={true}>Personal
                            Number</TableHeaderColumn>
                        <TableHeaderColumn dataField="status" dataSort={true}>Status</TableHeaderColumn>
                        <TableHeaderColumn dataField="city" dataSort={true}>City</TableHeaderColumn>
                    </BootstrapTable>
                </div>
            );
        }
    })
};


export var SaveStep = {
    name: 'Save',
    onSubmit: function (done) {
        var result = false;

        function Order(truck, waypoints, cargoes, drivers) {
            this.truck = truck;
            this.waypoints = waypoints;
            this.cargoes = cargoes;
            this.drivers = drivers;
        }

        var drivers = JSON.parse($.cookie('drivers')).map((dr) => {
            return dr.p_number;
        });
        var truck = JSON.parse($.cookie('truck')).map((tr) => {
            return tr.regnumber;
        })[0];
        var orderObject = new Order(truck, JSON.parse($.cookie('route')), JSON.parse($.cookie('cargoes')), drivers);
        $.ajax({
            contentType: "application/json",
            url: "/employee/orders/save",
            async: false,
            method: 'POST',
            data: JSON.stringify(orderObject),
            success: function () {
                result = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                result = false;
            }
        });
        if (result) {
            done();
        }
        return done(new Error('Error during saving order. Please check your order'));

    },

    component: React.createClass({
        displayName: 'component',
        render: function () {
            return (
                <div>
                    <label className="wizardHeaderLabel">If you wan't save order press on 'Onward'</label>
                </div>
            );
        }
    })
};

export var FinishStep = {
    name: 'Finish',
    component: React.createClass({
        finishSave: function () {
            location.reload();
        },
        displayName: 'component',
        render: function () {
            return (
                <div>
                    <label className="wizardHeaderLabel">Your oders was saved. Please, press 'finish'</label>
                    <Button
                        bsStyle="danger"
                        bsSize="sm"
                        id="finishButton"
                        onClick={this.finishSave}>
                        Finish
                    </Button>
                </div>
            );
        }
    })
};

import '../css/403.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );

export var ErrorForm = React.createClass({
    render: function() {
        return (
            <div className="errorForm">
                <form>
                    <span className="has-error">403</span>
                    <br/>
                    <span className="textError">Sorry! Access Denied</span>
                    <br/>
                </form>
            </div>
        );
    }
});
ReactDOM.render(<ErrorForm/>,document.body);
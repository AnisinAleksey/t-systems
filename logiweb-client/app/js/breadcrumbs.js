import '../css/breadcrumbs.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );

export var BreadCrumbsComponent = React.createClass({
    propTypes: {
        activeTab: React.PropTypes.string.isRequired
    },
    render: function() {
        return (
            <div>
                <ol className="breadcrumb" id = "navPan">
                    <li className={this.props.activeTab == 'trucks' ? 'active' : null}><a href="/employee/view/trucks">Trucks</a></li>
                    <li className={this.props.activeTab == 'drivers' ? 'active' : null}><a href="/employee/view/drivers">Drivers</a></li>
                    <li className={this.props.activeTab == 'orders' ? 'active' : null}><a href="/employee/view/orders">Orders</a></li>
                    <li className={this.props.activeTab == 'cargoes' ? 'active' : null}><a href="/employee/view/cargoes">Cargoes</a></li>
                    <p className="logout" id="logout"><a href="/logout">Logout</a></p>
                </ol>
            </div>
        );
    }
});
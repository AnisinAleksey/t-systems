var React = require('react');

export var DataListCities = React.createClass({
    propTypes: {
        idList: React.PropTypes.string.isRequired,
        cityList  : React.PropTypes.array.isRequired
    },
    render() {
        var options = [];
        this.props.cityList.forEach(function(city) {
            options.push(<option>{city}</option>);
        });
        return (
            <datalist id={this.props.idList}>{options}</datalist>
        );
    }
});
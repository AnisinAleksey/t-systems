import '../css/cargoes.css';
import '../css/breadcrumbs.css';
import '../css/bootstrap.min.css';
import '../css/react-bootstrap-table-all.min.css';

var React = require('react');
var ReactDOM = require('react-dom');
var $       = require( 'jquery' );
global.jQuery =  $;
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {BreadCrumbsComponent} from './breadcrumbs';
require( 'bootstrap' );

export var CargoesDataTable = React.createClass({
    getInitialState() {
        return {
            data: [],
            showModal: false
        };
    },
    componentWillMount: function(){
        var cargoesData = [];
        $.ajax({
            url: "/employee/cargoes",
            async: false,
            method: 'GET',
            success: function(data) {
                cargoesData = JSON.parse(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
            }
        });
        this.state.data = cargoesData;
    },
    render: function(){
        return (
            <div>
                <div>
                    <BootstrapTable data={this.state.data} striped={true} hover={true} pagination = {true} id = "cargoDataTable">
                        <TableHeaderColumn dataField="num" dataSort={true} isKey={true}>Number</TableHeaderColumn>
                        <TableHeaderColumn dataField="name" dataSort={true}>Name</TableHeaderColumn>
                        <TableHeaderColumn dataField="weight" dataSort={true}>Weight</TableHeaderColumn>
                        <TableHeaderColumn dataField="status" dataSort={true}>Status</TableHeaderColumn>
                    </BootstrapTable>
                </div>
            </div>
        )
    }
});


ReactDOM.render(<BreadCrumbsComponent activeTab='cargoes'/>, document.getElementById("breadcrumbsDiv"));
ReactDOM.render(<CargoesDataTable/>, document.getElementById("cargoDiv"));

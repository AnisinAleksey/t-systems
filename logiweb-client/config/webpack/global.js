'use strict';

// Depends
var path         = require('path');
var TextPlugin   = require('extract-text-webpack-plugin');
var HtmlPlugin   = require('html-webpack-plugin');

/**
 * Global webpack config
 * @param  {[type]} _path [description]
 * @return {[type]}       [description]
 */

let extractCSS = new TextPlugin('css/[name].css',{allChunks: true});

module.exports = function(_path) {
    // return objecy
    return {
        // entry points
        entry: {
            index : path.join(_path,'app','js','index'),
            login : path.join(_path,'app','js','login'),
            trucks : path.join(_path,'app','js','trucks'),
            drivers : path.join(_path,'app','js','drivers'),
            orders : path.join(_path,'app','js','orders'),
            cargoes : path.join(_path,'app','js','cargoes'),
            manager : path.join(_path,'app','js','manager'),
            driverPage : path.join(_path,'app','js','driverPage'),
            error : path.join(_path,'app','js','error'),
            403 : path.join(_path,'app','js','403'),
            404 : path.join(_path,'app','js','404')
        },

        // output system
        output: {
            path: path.join(_path, 'target'),
            filename: 'js/[name].js'
        },

        // resolves modules
        resolve: {
            root:  path.join(_path, 'node_modules'),
            extensions: ['', '.js'],
            modulesDirectories: ['node_modules']
        },

        // modules resolvers
        module: {
            loaders: [
                { test: /\.styl$/, loader: TextPlugin.extract('style-loader!css-loader') },
                { test: /\.js$/, loaders: ['babel'], include: path.join(_path,'app','js')},
                { test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,loader : 'file?name=fonts/[name].[ext]'},
                { test: /\.css$/, loader: extractCSS.extract("style-loader", "css-loader") },
                { test: /\.(jpe?g|png|gif)$/i,loader : 'file?name=img/[name].[ext]'},

            ]
        },


        // load plugins
        plugins: [
            extractCSS,
            new HtmlPlugin({  // Also generate a login.html
                title : 'Login',
                filename: 'login.html',
                chunks: ['login'],
                template: path.join(_path,'app','view_template','login-template.html')
            }),
            new HtmlPlugin({
                title: 'index',
                filename: 'index.html',
                chunks: ['index'],
                template: path.join(_path,'app','view_template','index-template.html')
            }),
            new HtmlPlugin({
                title: 'trucks',
                filename: 'trucks.html',
                chunks: ['trucks'],
                template: path.join(_path,'app','view_template','trucks-tepmlate.html')
            }),
            new HtmlPlugin({
                title: 'drivers',
                filename: 'drivers.html',
                chunks: ['drivers'],
                template: path.join(_path,'app','view_template','drivers-template.html')
            }),
            new HtmlPlugin({
                title: 'orders',
                filename: 'orders.html',
                chunks: ['orders'],
                template: path.join(_path,'app','view_template','orders-template.html')
            }),
            new HtmlPlugin({
                title: 'cargoes',
                filename: 'cargoes.html',
                chunks: ['cargoes'],
                template: path.join(_path,'app','view_template','cargoes-template.html')
            }),
            new HtmlPlugin({
                title: 'manager',
                filename: 'manager.html',
                chunks: ['manager'],
                template: path.join(_path,'app','view_template','manager-template.html')
            }),
            new HtmlPlugin({
                title: '404',
                filename: '404.html',
                chunks: ['404'],
                template: path.join(_path,'app','view_template','404-template.html')
            }),
            new HtmlPlugin({
                title: '403',
                filename: '403.html',
                chunks: ['403'],
                template: path.join(_path,'app','view_template','403-template.html')
            }),
            new HtmlPlugin({
                title: 'driverPage',
                filename: 'driverPage.html',
                chunks: ['driverPage'],
                template: path.join(_path,'app','view_template','driverPage-template.html')
            })
        ]
    };
};
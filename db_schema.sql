CREATE DATABASE IF NOT EXISTS logiweb ;

--------------------------------------------

CREATE TABLE city (
  id SERIAL      NOT NULL PRIMARY KEY ,
  name VARCHAR(255) NOT NULL UNIQUE,
  latitudine DECIMAL (9,2),
  longitudine DECIMAL (9,2)
);

CREATE TABLE truck (
  id            SERIAL        NOT NULL PRIMARY KEY,
  regnumber     VARCHAR(7) NOT NULL UNIQUE,
  count_drivers INT DEFAULT 0,
  capacity      INT DEFAULT 0,
  state         BOOLEAN,
  city_id       INT DEFAULT NULL REFERENCES city (id),
  order_id        INT DEFAULT NULL REFERENCES `order` (id)
);

CREATE TABLE `order` (
  id        SERIAL    NOT NULL PRIMARY KEY,
  number    BIGINT UNIQUE,
  state     BOOLEAN
);


DELIMITER //
CREATE TRIGGER order_before_insert
BEFORE INSERT
ON `order` FOR EACH ROW

  BEGIN
    DECLARE next_id INT;
    SET next_id = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME='order');
    SET NEW.number = next_id + 100000000;
  END;

DELIMITER ;


CREATE TABLE driver (
  id              SERIAL     NOT NULL PRIMARY KEY,
  name            VARCHAR(255),
  surname         VARCHAR(255),
  p_number        VARCHAR (50) NOT NULL UNIQUE,
  status          ENUM('REST', 'BUSY', 'DRIVING'),
  hours           INT DEFAULT 0,
  city_id         INT DEFAULT NULL REFERENCES city (id),
  truck_id        INT DEFAULT NULL REFERENCES truck (id),
  order_id        INT DEFAULT NULL REFERENCES `order` (id)
);

CREATE TABLE waypoint (
  id        SERIAL NOT NULL PRIMARY KEY,
  city_id   INT DEFAULT NULL REFERENCES city (id),
  order_id  INT DEFAULT NULL REFERENCES `order` (id),
  originalnum INT NOT NULL,
  CONSTRAINT uc_order UNIQUE (order_id,city_id)
);

CREATE TABLE cargo (
  id        SERIAL     NOT NULL PRIMARY KEY,
  num       BIGINT UNIQUE,
  name      VARCHAR(255),
  weight    DECIMAL(12,3),
  status    ENUM('PREPARED','SHIPPED','DELIVERED'),
  order_id  INT DEFAULT NULL REFERENCES `order` (id),
  waypoint_load_id  INT DEFAULT NULL REFERENCES waypoint (id),
  waypoint_unload_id INT DEFAULT NULL REFERENCES waypoint (id)
);

DELIMITER //
CREATE TRIGGER cargo_before_insert
BEFORE INSERT
ON cargo FOR EACH ROW

  BEGIN
    DECLARE next_id INT;
    SET next_id = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME='cargo');
    SET NEW.num = next_id + 100000000;
  END;

DELIMITER ;

CREATE TABLE users (
  username         varchar(15) not null primary key,
  password         varchar(15) not null
);

CREATE TABLE user_roles (
  username         varchar(15) not null,
  rolename         varchar(15) not null,
  primary key (username, rolename)
);

INSERT INTO users (username, password) VALUES ('admin', 'admin');
INSERT INTO user_roles (username,rolename) VALUES ('admin','MANAGER');
package com.tsystems.logiweb.exception;

/**
 * Created by Admin on 19.06.2016.
 */
public class LoginFailedException extends Exception {
    public LoginFailedException() {
        super("Login or password is incorrect");
    }
}

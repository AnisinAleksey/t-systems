package com.tsystems.logiweb.exception;

/**
 * Created by Admin on 22.06.2016.
 */
public class DriverHasOrderException extends Exception {
    public DriverHasOrderException() {
        super("Can't change status to REST because driver has order");
    }
}

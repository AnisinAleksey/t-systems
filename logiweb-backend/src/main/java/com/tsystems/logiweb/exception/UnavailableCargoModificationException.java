package com.tsystems.logiweb.exception;

/**
 * Created by Admin on 22.06.2016.
 */
public class UnavailableCargoModificationException extends Exception {

    public UnavailableCargoModificationException(String number) {
        super("This driver can't modify cargo with number " + number);
    }
}

package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.OrderDetailsDeserializer;
import com.tsystems.logiweb.dto.serializer.OrderDetailsSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by Admin on 21.06.2016.
 */
@JsonSerialize(using = OrderDetailsSerializer.class)
@JsonDeserialize(using = OrderDetailsDeserializer.class)
public class OrderDetailDTO {
    private List<DriverDTO> drivers;
    private List<WaypointDTO> waypoints;

    public List<DriverDTO> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<DriverDTO> drivers) {
        this.drivers = drivers;
    }

    public List<WaypointDTO> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<WaypointDTO> waypoints) {
        this.waypoints = waypoints;
    }
}

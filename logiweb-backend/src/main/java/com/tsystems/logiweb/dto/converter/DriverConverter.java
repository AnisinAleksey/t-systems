package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.DriverDTO;
import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.entities.enums.DriverStatus;
import com.tsystems.logiweb.service.CityService;
import com.tsystems.logiweb.service.DriverService;
import com.tsystems.logiweb.service.OrderService;
import com.tsystems.logiweb.service.TruckService;
import com.tsystems.logiweb.service.impl.CityServiceImpl;
import com.tsystems.logiweb.service.impl.DriverServiceImpl;
import com.tsystems.logiweb.service.impl.OrderServiceImpl;
import com.tsystems.logiweb.service.impl.TruckServiceImpl;
import com.tsystems.logiweb.util.StringUtil;

import java.util.Objects;

/**
 * Created by Admin on 18.06.2016.
 */
public class DriverConverter implements Converter<Driver, DriverDTO> {

    private final DriverService driverService = new DriverServiceImpl();
    private final CityService cityService = new CityServiceImpl();
    private final TruckService truckService = new TruckServiceImpl();
    private final OrderService orderService = new OrderServiceImpl();

    @Override
    public DriverDTO from(Driver driver) {
        DriverDTO driverDto = new DriverDTO();
        driverDto.setId(driver.getId());
        driverDto.setName(driver.getName());
        driverDto.setSurname(driver.getSurname());
        driverDto.setP_number(driver.getP_number());
        driverDto.setStatus(Objects.toString(driver.getStatus()));
        driverDto.setCity(driver.getCity() == null ? "" : driver.getCity().getName());
        driverDto.setTruck(driver.getTruck() == null ? "" : driver.getTruck().getRegnumber());
        driverDto.setHours(driver.getHours());
        driverDto.setOrder(driver.getOrder() == null ? null : driver.getOrder().getNumber());
        return driverDto;
    }

    @Override
    public Driver to(DriverDTO dto) throws Exception {
        Driver driver = new Driver();
        driver.setName(dto.getName());
        driver.setSurname(dto.getSurname());
        driver.setP_number(dto.getP_number());
        driver.setStatus(DriverStatus.valueOf(dto.getStatus()));
        driver.setHours(dto.getHours());
        if(StringUtil.isNotEmpty(dto.getCity())) {
            driver.setCity(cityService.getCityByName(dto.getCity()));
        }
        if(StringUtil.isNotEmpty(dto.getTruck()))
            driver.setTruck(truckService.getTruckByRegnumber(dto.getTruck()));
        else
            driverService.refreshTruck(driver);
        if(dto.getOrder() != null)
            driver.setOrder(orderService.getOrderByNumber(dto.getOrder()));
        else
            driverService.refreshOrder(driver);
        driverService.refreshId(driver);
        return driver;
    }
}

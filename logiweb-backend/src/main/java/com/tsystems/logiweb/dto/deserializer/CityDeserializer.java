package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.CityDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;

public class CityDeserializer extends JsonDeserializer<CityDTO> {
    @Override
    public CityDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        CityDTO city = new CityDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        city.setName(jsonNode.get("name").getValueAsText());
        city.setLatitudine(jsonNode.get("latitudine").getDoubleValue());
        city.setLongitudine(jsonNode.get("longitudine").getDoubleValue());
        return city;
    }
}

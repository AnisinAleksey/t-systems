package com.tsystems.logiweb.dto.converter.factory;

import com.tsystems.logiweb.dto.*;
import com.tsystems.logiweb.dto.converter.*;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Waypoint;

/**
 * Created by Admin on 18.06.2016.
 */
public class ConverterFactory {
    public static TruckConverter createTruckConverter() {
        return new TruckConverter();
    }

    public static DriverConverter createDriverConverter() {
        return new DriverConverter();
    }

    public static CargoConverter createCargoConverter() {
        return new CargoConverter();
    }

    public static CityConverter createCityConverter() {
        return new CityConverter();
    }

    public static Converter<Order, OrderDTO> createOrderConverter() {
        return new OrderConverter();
    }

    public static Converter<Waypoint, WaypointDTO> createWaypointConverter() {
        return new WaypontConverter();
    }

    public static Converter<OrderDTO, OrderInfoDTO> createOrderInfoConverter() {
        return new OrderInfoConverter();
    }

    public static Converter<DriverInfoDTO, DriverDTO> createDriverInfoDTOConverter() {
        return new DriverInfoConverter();
    }

}

package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.WaypointDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;

public class WaypointDeserializer extends JsonDeserializer<WaypointDTO> {
    @Override
    public WaypointDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        WaypointDTO waypoint = new WaypointDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        waypoint.setOriginalnum(Integer.valueOf(jsonNode.get("originalNum").getValueAsText()));
        waypoint.setCity(jsonNode.get("city").getValueAsText());
        waypoint.setOrder(jsonNode.get("order") == null ? null : Long.valueOf(jsonNode.getValueAsText()));
        return waypoint;
    }
}

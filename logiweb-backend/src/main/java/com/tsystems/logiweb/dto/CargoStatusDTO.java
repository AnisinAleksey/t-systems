package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.CargoStatusDeserializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

/**
 * Created by Admin on 22.06.2016.
 */
@JsonDeserialize(using = CargoStatusDeserializer.class)
public class CargoStatusDTO {

    private String number;
    private String status;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

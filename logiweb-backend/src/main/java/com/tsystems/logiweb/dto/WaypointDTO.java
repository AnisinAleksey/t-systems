package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.WaypointDeserializer;
import com.tsystems.logiweb.dto.serializer.WaypointSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@JsonSerialize(using = WaypointSerializer.class)
@JsonDeserialize(using = WaypointDeserializer.class)
public class WaypointDTO extends IdDTO  {

    @NotNull
    @Pattern(regexp = "/^\\d+$/")
    private Integer originalnum;

    private String city;

    private Long order;

    private String lat;

    private String lon;


    public WaypointDTO() {
    }

    public Integer getOriginalnum() {
        return originalnum;
    }

    public void setOriginalnum(Integer originalnum) {
        this.originalnum = originalnum;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "WaypointDTO{" +
                "originalnum=" + originalnum +
                ", city=" + city;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}

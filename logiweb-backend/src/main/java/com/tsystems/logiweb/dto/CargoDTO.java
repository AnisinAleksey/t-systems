package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.CargoDeserializer;
import com.tsystems.logiweb.dto.serializer.CargoSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@JsonSerialize(using = CargoSerializer.class)
@JsonDeserialize(using = CargoDeserializer.class)
public class CargoDTO extends IdDTO{

    private Long num;

    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String name;

    @Pattern(regexp = "/^\\d+$/")
    private Double weight;

    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String status;

    @Valid
    private WaypointDTO unloadWaypoint;

    @Valid
    private WaypointDTO loadWaypoint;

    public CargoDTO() {
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WaypointDTO getUnloadWaypoint() {
        return unloadWaypoint;
    }

    public void setUnloadWaypoint(WaypointDTO unloadWaypoint) {
        this.unloadWaypoint = unloadWaypoint;
    }

    public WaypointDTO getLoadWaypoint() {
        return loadWaypoint;
    }

    public void setLoadWaypoint(WaypointDTO loadWaypoint) {
        this.loadWaypoint = loadWaypoint;
    }
}

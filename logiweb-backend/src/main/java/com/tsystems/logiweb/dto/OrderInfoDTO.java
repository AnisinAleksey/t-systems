package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.OrderInfoDeserializer;
import com.tsystems.logiweb.dto.serializer.OrderInfoSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.Pattern;

/**
 * Created by Admin on 18.06.2016.
 */
@JsonSerialize(using = OrderInfoSerializer.class)
@JsonDeserialize(using = OrderInfoDeserializer.class)
public class OrderInfoDTO  {

    private Long number;

    @Pattern(regexp = "/^[a-zA-Z]{2}\\d{5}$/")
    private String truckRegnumber;

    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String state;

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getTruckRegnumber() {
        return truckRegnumber;
    }

    public void setTruckRegnumber(String truckRegnumber) {
        this.truckRegnumber = truckRegnumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

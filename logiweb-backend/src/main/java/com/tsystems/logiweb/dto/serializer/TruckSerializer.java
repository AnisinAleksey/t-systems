package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.TruckDTO;
import com.tsystems.logiweb.entities.Truck;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by Admin on 16.06.2016.
 */
public class TruckSerializer extends JsonSerializer<TruckDTO> {
    @Override
    public void serialize(TruckDTO truck, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("regnumber", truck.getRegnumber());
        jsonGenerator.writeNumberField("capacity", truck.getCapacity());
        jsonGenerator.writeNumberField("count_drivers", truck.getCount_drivers());
        jsonGenerator.writeStringField("state", truck.isState() ? "OK" : "BAD");
        jsonGenerator.writeObjectField("city", truck.getCity());
        jsonGenerator.writeEndObject();

        /**
         * {
         *      regnumber: "fx12313",
         *      capacity: 33,
         *      count_drivers: 123,
         *      ...
         *      city: {
         *          name: "fff",
         *          langitude: 12.1231231
         *      }
         *
         * }
         */
    }
}

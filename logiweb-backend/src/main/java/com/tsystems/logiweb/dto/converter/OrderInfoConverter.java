package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.OrderDTO;
import com.tsystems.logiweb.dto.OrderInfoDTO;

/**
 * Created by Admin on 18.06.2016.
 */
public class OrderInfoConverter implements Converter<OrderDTO, OrderInfoDTO> {
    @Override
    public OrderInfoDTO from(OrderDTO order) {
        OrderInfoDTO orderInfoDTO = new OrderInfoDTO();
        orderInfoDTO.setTruckRegnumber(order.getTruck());
        orderInfoDTO.setNumber(order.getNumber());
        orderInfoDTO.setState(order.getState());
        return orderInfoDTO;
    }

    @Override
    public OrderDTO to(OrderInfoDTO target) {
        return null;
    }
}

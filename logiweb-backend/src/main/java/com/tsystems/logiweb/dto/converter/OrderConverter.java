package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.OrderDTO;
import com.tsystems.logiweb.dto.WaypointDTO;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.service.OrderService;
import com.tsystems.logiweb.service.TruckService;
import com.tsystems.logiweb.service.impl.OrderServiceImpl;
import com.tsystems.logiweb.service.impl.TruckServiceImpl;
import com.tsystems.logiweb.util.StringUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Admin on 18.06.2016.
 */
public class OrderConverter implements Converter<Order, OrderDTO> {

    private final OrderService orderService = new OrderServiceImpl();
    private final TruckService truckService = new TruckServiceImpl();

    @Override
    public OrderDTO from(Order order) {
        OrderDTO dto = new OrderDTO();
        dto.setNumber(order.getNumber());
        dto.setTruck(order.getTruck() == null ? "" : order.getTruck().getRegnumber());
        dto.setState(order.getState() ? "COMPLETED" : "UNCOMPLETED");
        return dto;
    }

    @Override
    public Order to(OrderDTO dto) throws Exception {
        Order order = new Order();
        order.setNumber(dto.getNumber());
        order.setState(Objects.equals("COMPLETED",dto.getState()));
        if(StringUtil.isNotEmpty(dto.getTruck()))
            order.setTruck(truckService.getTruckByRegnumber(dto.getTruck()));
        else
            orderService.refreshTruck(order);
        if(dto.getWaypoints() != null) {
            Converter<Waypoint, WaypointDTO> waypointConverter = ConverterFactory.createWaypointConverter();
            List<Waypoint> waypoints = new LinkedList<>();
            for(WaypointDTO waypointDTO : dto.getWaypoints()) {
                waypoints.add(waypointConverter.to(waypointDTO));
            }
            order.setWaypoints(waypoints);
        }
        else
            orderService.refreshWaypoints(order);
        orderService.refreshId(order);
        return order;
    }
}

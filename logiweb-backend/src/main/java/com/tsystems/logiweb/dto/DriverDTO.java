package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.DriverDeserializer;
import com.tsystems.logiweb.dto.serializer.DriverSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@JsonSerialize(using = DriverSerializer.class)
@JsonDeserialize(using = DriverDeserializer.class)
public class DriverDTO extends IdDTO {

    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String surname;

    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String name;

    @Size(min = 5, max = 5)
    @Pattern(regexp = "/^\\d+$/")
    private Long p_number;

    private String status;

    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String city;

    @Pattern(regexp = "/^[a-zA-Z]{2}\\d{5}$/")
    private String truck;

    @Pattern(regexp = "/^\\d+$/")
    private Long order;

    @Pattern(regexp = "/^\\d+$/")
    private int hours;


    public DriverDTO() {

    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getP_number() {
        return p_number;
    }

    public void setP_number(Long p_number) {
        this.p_number = p_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

}

package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.serializer.DriverInfoSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;


@JsonSerialize(using = DriverInfoSerializer.class)
public class DriverInfoDTO {

    private Long personalNumber;
    private String truck;
    private Long orderNumber;
    private List<WaypointDTO> waypoints;
    private List<CoDriverInfoDTO> coDrivers;

    public Long getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(Long personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }

    public Long getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<WaypointDTO> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<WaypointDTO> waypoints) {
        this.waypoints = waypoints;
    }

    public List<CoDriverInfoDTO> getCoDrivers() {
        return coDrivers;
    }

    public void setCoDrivers(List<CoDriverInfoDTO> coDrivers) {
        this.coDrivers = coDrivers;
    }
}

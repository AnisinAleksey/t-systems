package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.CargoDTO;
import com.tsystems.logiweb.dto.WaypointDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class CargoDeserializer extends JsonDeserializer<CargoDTO> {
    @Override
    public CargoDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        CargoDTO cargo = new CargoDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        cargo.setNum(jsonNode.get("num") == null ? null : Long.valueOf(jsonNode.get("num").getValueAsText()));
        cargo.setName(jsonNode.get("name").getValueAsText());
        cargo.setWeight(jsonNode.get("weight") == null ? null : Double.valueOf(jsonNode.get("weight").getValueAsText()));
        ObjectMapper objectMapper = new ObjectMapper();
        cargo.setUnloadWaypoint(objectMapper.readValue(jsonNode.get("unload").toString(), WaypointDTO.class));
        cargo.setLoadWaypoint(objectMapper.readValue(jsonNode.get("load").toString(), WaypointDTO.class));
        return cargo;
    }
}

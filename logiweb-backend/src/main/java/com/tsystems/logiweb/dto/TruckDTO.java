package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.TruckDeserializer;
import com.tsystems.logiweb.dto.serializer.TruckSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@JsonSerialize(using = TruckSerializer.class)
@JsonDeserialize(using = TruckDeserializer.class)
public class TruckDTO extends IdDTO {

    @NotNull
    @Pattern(regexp = "/^[a-zA-Z]{2}\\d{5}$/")
    private String regnumber;

    @Pattern(regexp = "/^\\d+$/")
    private int count_drivers;

    @Pattern(regexp = "/^\\d+$/")
    private int capacity;

    private boolean state;

    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String city;

    public TruckDTO() {
    }

    public String getRegnumber() {
        return regnumber;
    }

    public void setRegnumber(String regnumber) {
        this.regnumber = regnumber;
    }

    public int getCount_drivers() {
        return count_drivers;
    }

    public void setCount_drivers(int count_drivers) {
        this.count_drivers = count_drivers;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}

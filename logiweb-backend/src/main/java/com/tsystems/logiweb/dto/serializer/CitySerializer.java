package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.CityDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

public class CitySerializer extends JsonSerializer<CityDTO> {
    @Override
    public void serialize(CityDTO city, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", city.getName());
        jsonGenerator.writeNumberField("weight", city.getLatitudine());
        jsonGenerator.writeNumberField("num", city.getLongitudine());
        jsonGenerator.writeEndObject();
    }
}

package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.OrderDeserializer;
import com.tsystems.logiweb.dto.serializer.OrderSerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.Pattern;
import java.util.List;

@JsonSerialize(using = OrderSerializer.class)
@JsonDeserialize(using = OrderDeserializer.class)
public class OrderDTO extends IdDTO {

    private Long number;

    private String state;

    @Pattern(regexp = "/^[a-zA-Z]{2}\\d{5}$/")
    private String truck;

    private List<String> drivers;

    private List<CargoDTO> cargoes;

    private List<WaypointDTO> waypoints;

    public OrderDTO() {

    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTruck() {
        return truck;
    }

    public void setTruck(String truck) {
        this.truck = truck;
    }


    public List<WaypointDTO> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<WaypointDTO> waypoints) {
        this.waypoints = waypoints;
    }

    public List<String> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<String> drivers) {
        this.drivers = drivers;
    }

    public List<CargoDTO> getCargoes() {
        return cargoes;
    }

    public void setCargoes(List<CargoDTO> cargoes) {
        this.cargoes = cargoes;
    }
}

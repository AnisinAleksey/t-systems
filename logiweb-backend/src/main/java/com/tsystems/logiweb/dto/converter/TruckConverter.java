package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.TruckDTO;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.service.CityService;
import com.tsystems.logiweb.service.TruckService;
import com.tsystems.logiweb.service.impl.CityServiceImpl;
import com.tsystems.logiweb.service.impl.TruckServiceImpl;
import com.tsystems.logiweb.util.StringUtil;

/**
 * Created by Admin on 18.06.2016.
 */
public class TruckConverter implements Converter<Truck, TruckDTO> {

    private TruckService truckService = new TruckServiceImpl();
    private CityService cityService = new CityServiceImpl();

    @Override
    public TruckDTO from(Truck truck) {
        TruckDTO dto = new TruckDTO();
        dto.setId(truck.getId());
        dto.setRegnumber(truck.getRegnumber());
        dto.setCount_drivers(truck.getCount_drivers());
        dto.setCapacity(truck.getCapacity());
        dto.setState(truck.isState());
        dto.setCity(truck.getCity() == null ? "" : truck.getCity().getName());
        return dto;
    }

    @Override
    public Truck to(TruckDTO dto) throws Exception {
        Truck truck = new Truck();
        truck.setId(dto.getId());
        truck.setRegnumber(dto.getRegnumber());
        truck.setCount_drivers(dto.getCount_drivers());
        truck.setCapacity(dto.getCapacity());
        truck.setState(dto.isState());
        if(StringUtil.isNotEmpty(dto.getCity())) {
            truck.setCity(cityService.getCityByName(dto.getCity()));
        }
        truckService.refreshOrder(truck);
        if(dto.getId() == null)
            truckService.refreshId(truck);
        return truck;
    }
}

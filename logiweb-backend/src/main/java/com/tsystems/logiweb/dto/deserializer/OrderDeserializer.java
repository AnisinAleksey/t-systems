package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.CargoDTO;
import com.tsystems.logiweb.dto.OrderDTO;
import com.tsystems.logiweb.dto.WaypointDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;

public class OrderDeserializer extends JsonDeserializer<OrderDTO> {
    @Override
    public OrderDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        OrderDTO order = new OrderDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        order.setNumber(jsonNode.get("number") == null ? null : Long.valueOf(jsonNode.get("number").getValueAsText()));
        order.setState(jsonNode.get("state") == null ? null : jsonNode.get("state").getValueAsText());
        order.setTruck(jsonNode.get("truck") == null ? null : jsonNode.get("truck").getValueAsText());
        ObjectMapper objectMapper = new ObjectMapper();
        String waypoints = jsonNode.get("waypoints") == null ? null : jsonNode.get("waypoints").toString();
        order.setWaypoints(waypoints == null ? null : Arrays.asList(objectMapper.readValue(waypoints, WaypointDTO[].class)));
        String cargoes = jsonNode.get("cargoes") == null ? null : jsonNode.get("cargoes").toString();
        order.setCargoes(cargoes == null ? null : Arrays.asList(objectMapper.readValue(cargoes, CargoDTO[].class)));
        String drivers = jsonNode.get("drivers") == null ? null : jsonNode.get("drivers").toString();
        order.setDrivers(drivers == null ? null : Arrays.asList(objectMapper.readValue(drivers, String[].class)));
        return order;
    }
}

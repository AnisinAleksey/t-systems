package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.serializer.CoDriverInfoSerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize(using = CoDriverInfoSerializer.class)
public class CoDriverInfoDTO {
    private Long personalNumber;

    public CoDriverInfoDTO() {}

    public CoDriverInfoDTO(Long personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Long getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(Long personalNumber) {
        this.personalNumber = personalNumber;
    }
}

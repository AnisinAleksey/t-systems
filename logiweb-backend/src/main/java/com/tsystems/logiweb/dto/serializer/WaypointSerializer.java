package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.WaypointDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

public class WaypointSerializer extends JsonSerializer<WaypointDTO> {
    @Override
    public void serialize(WaypointDTO waypoint, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("originalNum", waypoint.getOriginalnum());
        jsonGenerator.writeStringField("city", waypoint.getCity());
        jsonGenerator.writeStringField("lat", waypoint.getLat());
        jsonGenerator.writeStringField("lon", waypoint.getLon());
        jsonGenerator.writeStringField("order", waypoint.getOrder() == null ? "" : waypoint.getOrder().toString());
        jsonGenerator.writeEndObject();
    }
}

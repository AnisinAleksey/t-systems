package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.DriverDTO;
import com.tsystems.logiweb.dto.OrderDetailDTO;
import com.tsystems.logiweb.dto.WaypointDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Admin on 21.06.2016.
 */
public class OrderDetailsDeserializer extends JsonDeserializer<OrderDetailDTO> {

    @Override
    public OrderDetailDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        OrderDetailDTO order = new OrderDetailDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        ObjectMapper objectMapper = new ObjectMapper();
        String waypoints = jsonNode.get("waypoints") == null ? null : jsonNode.get("waypoints").toString();
        order.setWaypoints(waypoints == null ? null : Arrays.asList(objectMapper.readValue(waypoints, WaypointDTO[].class)));
        String drivers = jsonNode.get("drivers") == null ? null : jsonNode.get("drivers").toString();
        order.setDrivers(drivers == null ? null : Arrays.asList(objectMapper.readValue(drivers, DriverDTO[].class)));
        return order;
    }
}

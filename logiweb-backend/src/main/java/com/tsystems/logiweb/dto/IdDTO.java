package com.tsystems.logiweb.dto;

import org.codehaus.jackson.annotate.JsonIgnore;


public abstract class IdDTO  {

    @JsonIgnore
    private Long id;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

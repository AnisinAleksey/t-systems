package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.OrderInfoDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by Admin on 19.06.2016.
 */
public class OrderInfoSerializer extends JsonSerializer<OrderInfoDTO> {
    @Override
    public void serialize(OrderInfoDTO order, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("number", order.getNumber());
        jsonGenerator.writeStringField("state", order.getState());
        jsonGenerator.writeStringField("truck_id", order.getTruckRegnumber());
        jsonGenerator.writeEndObject();
    }
}

package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.OrderDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

public class OrderSerializer extends JsonSerializer<OrderDTO> {
    @Override
    public void serialize(OrderDTO order, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("number", order.getNumber());
        jsonGenerator.writeStringField("state", order.getState());
        jsonGenerator.writeStringField("truck_id", order.getTruck());
        jsonGenerator.writeEndObject();
    }
}

package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.CargoDTO;
import com.tsystems.logiweb.dto.WaypointDTO;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.Cargo;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.entities.enums.CargoStatus;
import com.tsystems.logiweb.service.CargoService;
import com.tsystems.logiweb.service.impl.CargoServiceImpl;

/**
 * Created by Admin on 18.06.2016.
 */
public class CargoConverter implements Converter<com.tsystems.logiweb.entities.Cargo,CargoDTO> {

    private CargoService cargoService = new CargoServiceImpl();

    @Override
    public CargoDTO from(Cargo cargo) {
        CargoDTO dto = new CargoDTO();
        dto.setId(cargo.getId());
        dto.setNum(cargo.getNum());
        dto.setName(cargo.getName());
        dto.setWeight(cargo.getWeight());
        dto.setStatus(cargo.getStatus() == null ? "" : cargo.getStatus().toString());
        Converter<Waypoint,WaypointDTO> waypointDTOConverter = ConverterFactory.createWaypointConverter();
        dto.setLoadWaypoint(waypointDTOConverter.from(cargo.getWaypointLoad()));
        dto.setUnloadWaypoint(waypointDTOConverter.from(cargo.getWaypointUnload()));
        return dto;
    }

    @Override
    public Cargo to(CargoDTO dto) throws Exception {
        Cargo cargo = new com.tsystems.logiweb.entities.Cargo();
        cargo.setId(dto.getId());
        cargo.setName(dto.getName());
        cargo.setWeight(dto.getWeight());
        cargo.setStatus(dto.getStatus() == null ? null : CargoStatus.valueOf(dto.getStatus()));
        Converter<Waypoint,WaypointDTO> waypointDTOConverter = ConverterFactory.createWaypointConverter();
        cargo.setWaypointLoad(waypointDTOConverter.to(dto.getLoadWaypoint()));
        cargo.setWaypointUnload(waypointDTOConverter.to(dto.getUnloadWaypoint()));
        if(dto.getId() != null)
            cargoService.refreshId(cargo);
        return cargo;
    }
}

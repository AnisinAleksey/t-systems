package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.CityDeserializer;
import com.tsystems.logiweb.dto.serializer.CitySerializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@JsonSerialize(using = CitySerializer.class)
@JsonDeserialize(using = CityDeserializer.class)
public class CityDTO extends IdDTO {

    @NotNull
    @Pattern(regexp = "/^[a-zA-Z]+$/")
    private String name;

    @DecimalMin("0.00") @DecimalMax("90.00")
    private double latitudine;

    @DecimalMin("0.00") @DecimalMax("180.00")
    private double longitudine;

    public CityDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitudine() {
        return latitudine;
    }

    public void setLatitudine(double latitudine) {
        this.latitudine = latitudine;
    }

    public double getLongitudine() {
        return longitudine;
    }

    public void setLongitudine(double longitudine) {
        this.longitudine = longitudine;
    }

}

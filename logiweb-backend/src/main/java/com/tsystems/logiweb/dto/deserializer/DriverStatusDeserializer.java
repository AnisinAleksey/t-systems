package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.DriverStatusDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;

/**
 * Created by Admin on 22.06.2016.
 */
public class DriverStatusDeserializer extends JsonDeserializer<DriverStatusDTO> {
    @Override
    public DriverStatusDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        DriverStatusDTO cargoStatusDTO = new DriverStatusDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        cargoStatusDTO.setStatus(jsonNode.get("status").getValueAsText());
        return cargoStatusDTO;
    }
}

package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.entities.enums.DriverStatus;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;


public class DriverForDriverDTO {

    @Size(min = 5, max = 5)
    @Pattern(regexp = "/^\\d+$/")
    private Long p_number;

    private List<Long> anotherDrivers;

    @Pattern(regexp = "/^[a-zA-Z]{2}\\d{5}$/")
    private String regnumber;

    @NotNull
    @Size(min = 10, max = 10)
    @Pattern(regexp = "/^\\d+$/")
    private Long number;

    private List<WaypointDTO> waypoints;

    public DriverForDriverDTO() {

    }

    public Long getP_number() {
        return p_number;
    }

    public void setP_number(Long p_number) {
        this.p_number = p_number;
    }

    public List<Long> getAnotherDrivers() {
        return anotherDrivers;
    }

    public void setAnotherDrivers(List<Long> anotherDrivers) {
        this.anotherDrivers = anotherDrivers;
    }

    public String getRegnumber() {
        return regnumber;
    }

    public void setRegnumber(String regnumber) {
        this.regnumber = regnumber;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public List<WaypointDTO> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<WaypointDTO> waypoints) {
        this.waypoints = waypoints;
    }

    @Override
    public String toString() {
        return "DriverForDriverDTO{" +
                "p_number=" + p_number +
                ", anotherDrivers=" + anotherDrivers +
                ", regnumber='" + regnumber + '\'' +
                ", number of order=" + number +
                ", waypoints=" + waypoints +
                '}';
    }
}

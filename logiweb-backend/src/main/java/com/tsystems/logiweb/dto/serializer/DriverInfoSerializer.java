package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.DriverInfoDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by Admin on 21.06.2016.
 */
public class DriverInfoSerializer extends JsonSerializer<DriverInfoDTO> {
    @Override
    public void serialize(DriverInfoDTO driver, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("p_number", driver.getPersonalNumber());
        jsonGenerator.writeStringField("regnumber", driver.getTruck() == null ? "NO ORDER" : driver.getTruck());
        jsonGenerator.writeStringField("number", driver.getOrderNumber() == null ? "NO ORDER" : String.valueOf(driver.getOrderNumber()));
        jsonGenerator.writeObjectField("coDrivers", driver.getCoDrivers());
        jsonGenerator.writeObjectField("waypoints", driver.getWaypoints());
        jsonGenerator.writeEndObject();
    }
}

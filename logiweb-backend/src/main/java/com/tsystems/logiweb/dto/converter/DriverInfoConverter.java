package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.CoDriverInfoDTO;
import com.tsystems.logiweb.dto.DriverDTO;
import com.tsystems.logiweb.dto.DriverInfoDTO;
import com.tsystems.logiweb.facade.OrderFacade;
import com.tsystems.logiweb.facade.impl.OrderFacadeImpl;
import com.tsystems.logiweb.service.DriverService;
import com.tsystems.logiweb.service.impl.DriverServiceImpl;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by Admin on 21.06.2016.
 */
public class DriverInfoConverter implements Converter<DriverInfoDTO, DriverDTO> {

    private DriverService driverService = new DriverServiceImpl();
    private OrderFacade orderFacade = new OrderFacadeImpl();

    @Override
    public DriverDTO from(DriverInfoDTO driverDTO) {
        return null;
    }

    @Override
    public DriverInfoDTO to(DriverDTO driverDTO) throws Exception {
        DriverInfoDTO driverInfoDTO = new DriverInfoDTO();
        driverInfoDTO.setPersonalNumber(driverDTO.getP_number());
        driverInfoDTO.setTruck(driverDTO.getTruck());
        driverInfoDTO.setCoDrivers(driverService.getCoDriversByLicence(driverDTO.getP_number())
                .stream().map(CoDriverInfoDTO::new).collect(Collectors.toList()));
        driverInfoDTO.setOrderNumber(driverDTO.getOrder());
        if(driverDTO.getOrder() != null)
            driverInfoDTO.setWaypoints(orderFacade.getWaypointsByOrderNumber(driverDTO.getOrder().toString()));
        else
            driverInfoDTO.setWaypoints(new ArrayList<>());
        return driverInfoDTO;
    }
}

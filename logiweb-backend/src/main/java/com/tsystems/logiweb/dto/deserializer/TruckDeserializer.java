package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.TruckDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;
import java.util.Objects;

/**
 * Created by Admin on 16.06.2016.
 */
public class TruckDeserializer extends JsonDeserializer<TruckDTO> {
    @Override
    public TruckDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        TruckDTO truck = new TruckDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        truck.setRegnumber(jsonNode.get("regnumber").getValueAsText());
        truck.setCapacity(Integer.parseInt(jsonNode.get("capacity").getValueAsText()));
        truck.setCount_drivers(Integer.parseInt(jsonNode.get("count_drivers").getValueAsText()));
        truck.setState(Objects.equals("OK", jsonNode.get("state").getValueAsText()));
        truck.setCity(jsonNode.get("city").getValueAsText());
        return truck;
    }
}

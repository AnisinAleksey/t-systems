package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.CargoStatusDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;

/**
 * Created by Admin on 22.06.2016.
 */
public class CargoStatusDeserializer extends JsonDeserializer<CargoStatusDTO> {
    @Override
    public CargoStatusDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        CargoStatusDTO cargoStatusDTO = new CargoStatusDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        cargoStatusDTO.setNumber(jsonNode.get("number").getValueAsText());
        cargoStatusDTO.setStatus(jsonNode.get("status").getValueAsText());
        return cargoStatusDTO;
    }
}

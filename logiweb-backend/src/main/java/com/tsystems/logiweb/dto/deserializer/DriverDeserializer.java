package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.DriverDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;

public class DriverDeserializer extends JsonDeserializer<DriverDTO> {
    public DriverDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        DriverDTO driver = new DriverDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        driver.setP_number(Long.valueOf(jsonNode.get("p_number").getValueAsText()));
        driver.setName(jsonNode.get("name").getValueAsText());
        driver.setSurname(jsonNode.get("surname").getValueAsText());
        driver.setCity(jsonNode.get("city").getValueAsText());
        driver.setStatus(jsonNode.get("status").getValueAsText());
        return driver;
    }
}

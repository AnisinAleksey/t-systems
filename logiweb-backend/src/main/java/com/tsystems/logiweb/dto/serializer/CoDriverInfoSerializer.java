package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.CoDriverInfoDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by Admin on 21.06.2016.
 */
public class CoDriverInfoSerializer extends JsonSerializer<CoDriverInfoDTO> {
    @Override
    public void serialize(CoDriverInfoDTO driver, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("p_number", driver.getPersonalNumber());
        jsonGenerator.writeEndObject();
    }
}

package com.tsystems.logiweb.dto;

import com.tsystems.logiweb.dto.deserializer.DriverStatusDeserializer;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

/**
 * Created by Admin on 22.06.2016.
 */
@JsonDeserialize(using = DriverStatusDeserializer.class)
public class DriverStatusDTO {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

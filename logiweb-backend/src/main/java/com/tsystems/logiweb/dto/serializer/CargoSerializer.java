package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.CargoDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

public class CargoSerializer extends JsonSerializer<CargoDTO> {
    @Override
    public void serialize(CargoDTO cargo, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("num", cargo.getNum());
        jsonGenerator.writeStringField("name", cargo.getName());
        jsonGenerator.writeNumberField("weight", cargo.getWeight());
        jsonGenerator.writeStringField("status", cargo.getStatus());
        jsonGenerator.writeObjectField("unload",cargo.getUnloadWaypoint());
        jsonGenerator.writeObjectField("load",cargo.getLoadWaypoint());
        jsonGenerator.writeEndObject();
    }
}

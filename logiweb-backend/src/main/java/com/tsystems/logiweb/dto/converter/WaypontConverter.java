package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.WaypointDTO;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.service.CityService;
import com.tsystems.logiweb.service.OrderService;
import com.tsystems.logiweb.service.WaypointService;
import com.tsystems.logiweb.service.impl.CityServiceImpl;
import com.tsystems.logiweb.service.impl.OrderServiceImpl;
import com.tsystems.logiweb.service.impl.WaypointServiceImpl;
import com.tsystems.logiweb.util.StringUtil;

/**
 * Created by Admin on 19.06.2016.
 */
public class WaypontConverter implements Converter<Waypoint, WaypointDTO> {

    private WaypointService waypointService = new WaypointServiceImpl();
    private OrderService orderService = new OrderServiceImpl();
    private CityService cityService = new CityServiceImpl();

    @Override
    public WaypointDTO from(Waypoint waypoint) {
        WaypointDTO dto = new WaypointDTO();
        dto.setId(waypoint.getId());
        dto.setCity(waypoint.getCity() == null ? "" : waypoint.getCity().getName());
        dto.setLat(waypoint.getCity() == null ? "" : String.valueOf(waypoint.getCity().getLatitudine()));
        dto.setLon(waypoint.getCity() == null ? "" : String.valueOf(waypoint.getCity().getLongitudine()));
        dto.setOriginalnum(waypoint.getOriginalNumber());
        dto.setOrder(waypoint.getOrder() == null ? null : waypoint.getOrder().getNumber());
        return dto;
    }

    @Override
    public Waypoint to(WaypointDTO dto) throws Exception {
        Waypoint waypoint = new Waypoint();
        waypoint.setId(dto.getId());
        waypoint.setOriginalNumber(dto.getOriginalnum());
        if(StringUtil.isNotEmpty(dto.getCity()))
            waypoint.setCity(cityService.getCityByName(dto.getCity()));
        if(dto.getOrder() != null)
            waypoint.setOrder(orderService.getOrderByNumber(dto.getOrder()));
        if(dto.getId() == null)
            waypointService.refreshId(waypoint);
        return waypoint;
    }
}

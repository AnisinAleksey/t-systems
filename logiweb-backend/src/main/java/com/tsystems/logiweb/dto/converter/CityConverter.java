package com.tsystems.logiweb.dto.converter;

import com.tsystems.logiweb.dto.CityDTO;
import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.service.CityService;
import com.tsystems.logiweb.service.impl.CityServiceImpl;

/**
 * Created by Admin on 18.06.2016.
 */
public class CityConverter implements Converter<com.tsystems.logiweb.entities.City, CityDTO> {

    private CityService cityService = new CityServiceImpl();

    @Override
    public CityDTO from(City city) {
        CityDTO dto = new CityDTO();
        dto.setId(city.getId());
        dto.setName(city.getName());
        dto.setLatitudine(city.getLatitudine());
        dto.setLongitudine(city.getLongitudine());
        return dto;
    }

    @Override
    public City to(CityDTO dto) throws Exception {
        City city = new City();
        city.setName(dto.getName());
        city.setLatitudine(dto.getLatitudine());
        city.setLongitudine(dto.getLongitudine());
        cityService.refreshId(city);
        return city;
    }
}

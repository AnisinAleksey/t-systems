package com.tsystems.logiweb.dto.converter;

/**
 * Created by Admin on 18.06.2016.
 */
public interface Converter<S, T> {
    T from(S source);
    S to(T target) throws Exception;
}

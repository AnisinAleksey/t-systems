package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.OrderDetailDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

/**
 * Created by Admin on 21.06.2016.
 */
public class OrderDetailsSerializer extends JsonSerializer<OrderDetailDTO> {
    @Override
    public void serialize(OrderDetailDTO orderDetailDTO, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectField("drivers", orderDetailDTO.getDrivers());
        jsonGenerator.writeObjectField("waypoints", orderDetailDTO.getWaypoints());
        jsonGenerator.writeEndObject();

    }
}

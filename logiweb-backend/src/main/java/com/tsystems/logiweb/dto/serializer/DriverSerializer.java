package com.tsystems.logiweb.dto.serializer;

import com.tsystems.logiweb.dto.DriverDTO;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;

public class DriverSerializer extends JsonSerializer<DriverDTO> {
    @Override
    public void serialize(DriverDTO driver, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("p_number", driver.getP_number());
        jsonGenerator.writeStringField("name", driver.getName());
        jsonGenerator.writeStringField("surname", driver.getSurname());
        jsonGenerator.writeStringField("status", driver.getStatus());
        jsonGenerator.writeStringField("city", driver.getCity());
        jsonGenerator.writeEndObject();
    }
}

package com.tsystems.logiweb.dto.deserializer;

import com.tsystems.logiweb.dto.OrderInfoDTO;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

import java.io.IOException;

/**
 * Created by Admin on 19.06.2016.
 */
public class OrderInfoDeserializer extends JsonDeserializer<OrderInfoDTO> {

    @Override
    public OrderInfoDTO deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        OrderInfoDTO order = new OrderInfoDTO();
        JsonNode jsonNode = jsonParser.readValueAsTree();
        order.setNumber(jsonNode.get("number").getLongValue());
        order.setState(jsonNode.get("state").getValueAsText());
        order.setTruckRegnumber(jsonNode.get("truck_id").getValueAsText());
        return order;
    }
}

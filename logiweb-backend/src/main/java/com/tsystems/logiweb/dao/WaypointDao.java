package com.tsystems.logiweb.dao;


import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Waypoint;

import java.util.List;

public interface WaypointDao extends JpaDao<Waypoint> {
    Waypoint findWaypointByOrderCity(Order order, City city);
    List<Waypoint> getListOfWaypoint(Order order);
}

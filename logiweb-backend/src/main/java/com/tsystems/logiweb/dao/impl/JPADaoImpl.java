package com.tsystems.logiweb.dao.impl;

import com.tsystems.logiweb.dao.JpaDao;

import javax.persistence.EntityManager;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Алексей on 04.06.2016.
 */
public class JPADaoImpl<T> implements JpaDao<T> {

    protected EntityManager em;

    private Class<T> persistentClass;

    public JPADaoImpl() {
        this(null);
    }

    @SuppressWarnings("unchecked")
    public JPADaoImpl(EntityManager em) {
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
        this.em = em;
    }

    @Override
    public List<T> getAll() {
        return em.createQuery("SELECT obj FROM " + persistentClass.getSimpleName() + " obj", this.persistentClass).getResultList();
    }

    @Override
    public Void delete(T entity) {
        em.remove(em.contains(entity) ? entity : em.merge(entity));
        return null;
    }

    @Override
    public Void update(T entity) {
        em.merge(entity);
        return null;
    }

    @Override
    public Void insert(T entity) {
        em.persist(entity);
        return null;
    }


    @Override
    public T get(Long id) {
        return em.find(persistentClass,id);
    }
}

package com.tsystems.logiweb.dao;

import com.tsystems.logiweb.entities.Driver;

import java.util.List;

/**
 * Created by Алексей on 04.06.2016.
 */
public interface DriverDao extends JpaDao<Driver> {

    Driver getDriverByLicence(long licence);
    List<Long> getCoDriversByLicence(long licence);
    List<Driver> freeDrivers();
}

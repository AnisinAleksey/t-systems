package com.tsystems.logiweb.dao;


import com.tsystems.logiweb.entities.Cargo;

public interface CargoDao extends JpaDao<Cargo> {
    Cargo findCargoByNumber(Long number);
}

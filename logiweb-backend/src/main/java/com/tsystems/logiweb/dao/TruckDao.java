package com.tsystems.logiweb.dao;

import com.tsystems.logiweb.entities.Truck;

import java.util.List;

public interface TruckDao extends JpaDao<Truck> {
    List<Truck> truckInAvailableCondition();
    List<Truck> performOrder ();
    Truck getTruckByRegnumber(String regnumber);

}

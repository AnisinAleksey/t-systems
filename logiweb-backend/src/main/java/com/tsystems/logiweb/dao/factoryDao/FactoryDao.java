package com.tsystems.logiweb.dao.factoryDao;

import com.tsystems.logiweb.dao.*;
import com.tsystems.logiweb.dao.impl.*;

import javax.persistence.EntityManager;

public class FactoryDao {
    public static CargoDao createCargoDao(EntityManager entityManager) {
        return new CargoDaoImpl(entityManager);
    }

    public static CityDao createCityDao(EntityManager entityManager) {
        return new CityDaoImpl(entityManager);
    }

    public static DriverDao createDriverDao(EntityManager entityManager) {
        return new DriverDaoImpl(entityManager);
    }

    public static OrderDao createOrderDao(EntityManager entityManager) {
        return new OrderDaoImpl(entityManager);
    }

    public static TruckDao createTruckDao(EntityManager entityManager) {
        return new TruckDaoImpl(entityManager);
    }

    public static WaypointDao createWaypointDao(EntityManager entityManager) {
        return new WaypointDaoImpl(entityManager);
    }

    public static UserDao createUserDao(EntityManager entityManager) {
        return new UserDaoImpl(entityManager);
    }

}
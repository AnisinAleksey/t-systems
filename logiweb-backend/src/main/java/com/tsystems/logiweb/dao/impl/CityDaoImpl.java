package com.tsystems.logiweb.dao.impl;

import com.tsystems.logiweb.dao.CityDao;
import com.tsystems.logiweb.entities.City;

import javax.persistence.EntityManager;
import java.util.List;

public class CityDaoImpl extends JPADaoImpl<City> implements CityDao {

    public CityDaoImpl(EntityManager em) {
        super(em);
    }


    @Override
    public List<City> searchByName(String likeName, int limit) {
        return em.createQuery("SELECT c FROM City c WHERE c.name LIKE :name", City.class).setMaxResults(limit)
                .setParameter("name", String.join("",likeName,"%"))
                .getResultList();
    }

    @Override
    public City getCityByName(String name) {
        return em.createQuery("SELECT c FROM City c WHERE c.name=:name", City.class)
                .setParameter("name",name)
                .getSingleResult();
    }

}

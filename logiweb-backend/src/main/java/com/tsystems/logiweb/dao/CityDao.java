package com.tsystems.logiweb.dao;


import com.tsystems.logiweb.entities.City;

import java.util.List;

public interface CityDao extends JpaDao<City>{
    List<City> searchByName(String likeName, int limit);
    City getCityByName(String name);
}

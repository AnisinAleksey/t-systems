package com.tsystems.logiweb.dao;

/**
 * Created by Admin on 21.06.2016.
 */
public interface UserDao {
    String registerUser(String user, String password, String role);
}

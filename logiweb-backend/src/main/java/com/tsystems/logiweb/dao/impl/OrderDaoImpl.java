package com.tsystems.logiweb.dao.impl;

import com.tsystems.logiweb.dao.OrderDao;
import com.tsystems.logiweb.entities.Order;

import javax.persistence.EntityManager;

public class OrderDaoImpl extends JPADaoImpl<Order> implements OrderDao {

    public OrderDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public Void insert(Order order) {
        super.insert(order);
        em.refresh(order);
        em.getEntityManagerFactory().getCache().evictAll();
        return null;
    }

    @Override
    public boolean isMade(Order order) {
        return em
                .createQuery("SELECT o.state from Order o where o.id = :id", Boolean.class)
                .setParameter("id", order.getId())
                .getSingleResult();
        }

    @Override
    public Order getOrderByNumber(Long number) {
        return em.createQuery("SELECT t FROM Order t WHERE t.number =:number", Order.class)
                .setParameter("number",number).getSingleResult();
    }


}

package com.tsystems.logiweb.dao.impl;

import com.tsystems.logiweb.dao.WaypointDao;
import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Waypoint;

import javax.persistence.EntityManager;
import java.util.List;

public class WaypointDaoImpl extends JPADaoImpl<Waypoint> implements WaypointDao {

    public WaypointDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public Waypoint findWaypointByOrderCity(Order order, City city) {
        return em.createQuery("SELECT w FROM Waypoint w WHERE w.order=:ord AND w.city=:city", Waypoint.class)
                .setParameter("ord", order).setParameter("city",city).getSingleResult();
    }

    @Override
    public List<Waypoint> getListOfWaypoint(Order order) {
        return em.createQuery("SELECT w FROM Waypoint w WHERE w.order.id = :id", Waypoint.class)
                .setParameter("id", order.getId())
                .getResultList();
    }
}

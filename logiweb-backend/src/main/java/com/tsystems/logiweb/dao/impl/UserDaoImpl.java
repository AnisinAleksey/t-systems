package com.tsystems.logiweb.dao.impl;

import com.tsystems.logiweb.dao.UserDao;

import javax.persistence.EntityManager;

/**
 * Created by Admin on 21.06.2016.
 */
public class UserDaoImpl implements UserDao {

    private EntityManager em;

    public UserDaoImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public String registerUser(String user, String password, String role) {
        em.createNativeQuery("INSERT INTO users (username, password) VALUES (:user, :password)")
                .setParameter("user",user)
                .setParameter("password",password)
                .executeUpdate();
        em.createNativeQuery("INSERT INTO user_roles (username, rolename) VALUES (:user, :role)")
                .setParameter("user",user)
                .setParameter("role",role)
                .executeUpdate();
        return password;
    }
}

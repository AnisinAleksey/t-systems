package com.tsystems.logiweb.dao.impl;

import com.tsystems.logiweb.dao.CargoDao;
import com.tsystems.logiweb.entities.Cargo;

import javax.persistence.EntityManager;
import java.util.List;

public class CargoDaoImpl extends JPADaoImpl<Cargo> implements CargoDao {

    public CargoDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public Void insert(Cargo cargo) {
        super.insert(cargo);
        em.refresh(cargo);
        em.getEntityManagerFactory().getCache().evictAll();
        return null;
    }

    @Override
    public Cargo findCargoByNumber(Long number) {
        return em
                .createQuery("SELECT c from Cargo c where c.num = :num", Cargo.class)
                .setParameter("num", number)
                .getSingleResult();
    }

}

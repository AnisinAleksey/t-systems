package com.tsystems.logiweb.dao;

import java.util.List;

public interface JpaDao<T> {
    List<T> getAll();
    Void delete(T entity);
    Void update(T entity);
    Void insert(T entity);
    T get(Long id);
}

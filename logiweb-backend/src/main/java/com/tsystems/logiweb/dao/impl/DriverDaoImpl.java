package com.tsystems.logiweb.dao.impl;

import com.tsystems.logiweb.dao.DriverDao;
import com.tsystems.logiweb.entities.Driver;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Алексей on 04.06.2016.
 */
public class DriverDaoImpl extends JPADaoImpl<Driver> implements DriverDao {

    public DriverDaoImpl() {}

    public DriverDaoImpl(EntityManager em) {
        super(em);
    }

    @Override
    public Driver getDriverByLicence(long licence) {
        return em.createQuery("SELECT d FROM Driver d WHERE d.p_number = :licence", Driver.class)
                .setParameter("licence", licence)
                .getSingleResult();
    }

    @Override
    public List<Long> getCoDriversByLicence(long licence) {
        return em
                .createQuery("SELECT d.p_number FROM Driver d WHERE d.truck.id = (SELECT d.truck.id FROM Driver d WHERE d.p_number = :licence) AND NOT d.p_number = :licence", Long.class)
                .setParameter("licence", licence)
                .getResultList();
    }


    @Override
    public List<Driver> freeDrivers() {
        return em
                .createQuery("SELECT d FROM Driver d WHERE d.status = 'REST'", Driver.class)
                .getResultList();
    }
}

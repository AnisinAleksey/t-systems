package com.tsystems.logiweb.dao.impl;


import com.tsystems.logiweb.dao.TruckDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.entities.Truck;

import javax.persistence.EntityManager;
import java.util.List;

public class TruckDaoImpl extends JPADaoImpl<Truck> implements TruckDao{

    public TruckDaoImpl(EntityManager em) {
        super(em);
    }


    @Override
    public List<Truck> performOrder() {
        return em
                .createQuery("SELECT o.truck FROM Order o", Truck.class)
                .getResultList();
    }

    @Override
    public Truck getTruckByRegnumber(String regnumber) {
        return em.createQuery("SELECT t FROM Truck t WHERE t.regnumber =:regnumber", Truck.class)
          .setParameter("regnumber",regnumber).getSingleResult();
    }

    @Override
    public List<Truck> truckInAvailableCondition(){
        return em
                .createQuery("SELECT t FROM Truck t WHERE t.state = true", Truck.class)
                .getResultList();
    }

}

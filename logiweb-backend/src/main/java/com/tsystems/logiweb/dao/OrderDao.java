package com.tsystems.logiweb.dao;


import com.tsystems.logiweb.entities.Order;

public interface OrderDao extends JpaDao<Order> {
    boolean isMade(Order order);
    Order getOrderByNumber(Long number);
}

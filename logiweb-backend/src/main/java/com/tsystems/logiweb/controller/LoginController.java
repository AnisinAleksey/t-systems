package com.tsystems.logiweb.controller;

import com.tsystems.logiweb.exception.LoginFailedException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 14.06.2016.
 */
@WebServlet(name = "LoginController", urlPatterns = {"/login"})
public class LoginController extends AbstractController {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(() -> processGet(request, response), response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> doLogin(request,response), response);
    }

    private void processGet(HttpServletRequest request, HttpServletResponse response) throws Exception{
        if(request.isUserInRole("MANAGER") || request.isUserInRole("DRIVER"))
            response.sendRedirect("/");
        else
            sendResource("login.html",request,response);
    }

    private void doLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            String user = request.getParameter("username");
            String password = request.getParameter("password");
            request.getSession();
            request.login(user, password);
            response.sendRedirect("/");
        } catch (ServletException e) {
            throw new LoginFailedException();
        }
    }
}

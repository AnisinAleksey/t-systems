package com.tsystems.logiweb.controller.manager;

import com.tsystems.logiweb.controller.AbstractController;
import com.tsystems.logiweb.dto.CargoDTO;
import com.tsystems.logiweb.facade.CargoFacade;
import com.tsystems.logiweb.facade.impl.CargoFacadeImpl;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 21.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"MANAGER"}))
@WebServlet(name = "CargoServletController", urlPatterns = {"/employee/cargoes/*"})
public class CargoController extends AbstractController {

    private final CargoFacade cargoFacade = new CargoFacadeImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> getAllCargoes(response),response);
    }

    private void getAllCargoes(HttpServletResponse response) throws Exception {
        List<CargoDTO> cargoes = cargoFacade.getAll();
        sendAsJson(response,cargoes);
    }
}

package com.tsystems.logiweb.controller.manager;

import com.tsystems.logiweb.controller.AbstractController;
import com.tsystems.logiweb.dto.TruckDTO;
import com.tsystems.logiweb.facade.TruckFacade;
import com.tsystems.logiweb.facade.impl.TruckFacadeImpl;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 15.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"MANAGER"}))
@WebServlet(name = "TruckServletController", urlPatterns = {"/employee/trucks", "/employee/trucks/*"})
public class TruckController extends AbstractController {

    private TruckFacade truckFacade = new TruckFacadeImpl();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> getAllTrucks(response),response);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> updateTruck(request,response),response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> createTruck(request,response),response);
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> deleteTruck(request,response), response);
    }

    private void updateTruck(HttpServletRequest request, HttpServletResponse response) throws Exception {
        TruckDTO newTruck = readObject(request,TruckDTO.class);
        truckFacade.update(newTruck);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private void createTruck(HttpServletRequest request, HttpServletResponse response) throws Exception {
        TruckDTO truck = readObject(request,TruckDTO.class);
        truckFacade.insert(truck);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private void deleteTruck(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String reg = request.getPathInfo().split("/")[1];
        TruckDTO truck = truckFacade.getTruckByRegnumber(reg);
        truckFacade.delete(truck);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private void getAllTrucks(HttpServletResponse response) throws Exception {
        List<TruckDTO> trucks = truckFacade.getAll();
        sendAsJson(response,trucks);
    }

}

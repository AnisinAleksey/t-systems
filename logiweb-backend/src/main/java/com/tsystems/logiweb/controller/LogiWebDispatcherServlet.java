package com.tsystems.logiweb.controller;

import com.tsystems.logiweb.engine.PersistenceManager;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Алексей on 04.06.2016.
 */
@WebServlet(name = "LogiwebServletDispatcher", urlPatterns = {"/"})
public class LogiWebDispatcherServlet extends AbstractController {

    @Override
    public void init() {
        PersistenceManager.getInstance().init();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        handle(() -> processGet(request,response),response);
    }

    private void processGet(HttpServletRequest request, HttpServletResponse response) throws Exception{
        if(request.isUserInRole("MANAGER"))
            response.sendRedirect("/employee");
        else if(request.isUserInRole("DRIVER"))
            response.sendRedirect("/driver");
        else
            response.sendRedirect("/login");
    }

    @Override
    public void destroy() {
        PersistenceManager.getInstance().destroy();
    }
}

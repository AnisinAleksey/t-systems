package com.tsystems.logiweb.controller;

import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by Admin on 13.06.2016.
 */
@WebServlet(name = "ResourceController", urlPatterns = {"*.css", "*.js", "*.eot","*.svg","*.ttf","*.woff", "*.woff2"})
public class ResourceController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(ResourceController.class);

    private Properties resourceMapping = new Properties();

    @Override
    public void init() {
        try {
            resourceMapping.load(getServletContext().getResourceAsStream(String.join("/","/WEB-INF", "classes", "resources-mapping-folder.properties")));
        } catch (IOException e) {
            LOG.error(e,e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> sendResource(getResourcePath(request),request,response),response);
    }

    private String getResourcePath(HttpServletRequest request) {
        String[] fullPathParts = request.getServletPath().split("\\.");
        String extension = fullPathParts[fullPathParts.length-1];
        String folderName = "";
        for (Map.Entry entry : resourceMapping.entrySet()) {
            if (isExtensionSupport(Objects.toString(entry.getValue()), extension)) {
                folderName = Objects.toString(entry.getKey());
                break;
            }
        }
        String fileName = getFileName(request);
        return String.join("/",folderName, fileName);
    }

    private String getFileName(HttpServletRequest request) {
        String[] fullPathParts = request.getServletPath().split("/");
        return fullPathParts[fullPathParts.length-1];
    }

    private boolean isExtensionSupport(String valueProperty, String extension) {
        for(String ext : valueProperty.split(";")) {
            if(Objects.equals(ext,extension)) {
                return true;
            }
        }
        return false;
    }
}

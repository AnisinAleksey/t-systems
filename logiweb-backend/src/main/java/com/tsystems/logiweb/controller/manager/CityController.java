package com.tsystems.logiweb.controller.manager;

/**
 * Created by Admin on 16.06.2016.
 */

import com.tsystems.logiweb.controller.AbstractController;
import com.tsystems.logiweb.dto.CityDTO;
import com.tsystems.logiweb.facade.CityFacade;
import com.tsystems.logiweb.facade.impl.CityFacadeImpl;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@ServletSecurity(@HttpConstraint(rolesAllowed={"MANAGER"}))
@WebServlet(name = "CityServletController", urlPatterns = {"/employee/cities/*"})
public class CityController extends AbstractController {

    private CityFacade cityFacade = new CityFacadeImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> processGet(request,response),response);
    }

    private void processGet(HttpServletRequest request, HttpServletResponse response) throws Exception {
        switch (ActionURL.fromUrl(request.getPathInfo())) {
            case DEFAULT:
                sendAsJson(response,cityFacade.getAll());
                break;
            case SEARCH:
                sendAsJson(response,searchCitiesByName(getQueryParams(request)));
                break;
        }
    }

    private List<String> searchCitiesByName(Map<String, String> params) throws Exception {
        List<String> cities = new LinkedList<>();
        for(CityDTO city : cityFacade.searchByName(params.get("phrase"))) {
            cities.add(city.getName());
        }
        return cities;
    }


}

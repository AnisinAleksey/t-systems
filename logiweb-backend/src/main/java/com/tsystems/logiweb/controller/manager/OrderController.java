package com.tsystems.logiweb.controller.manager;

import com.tsystems.logiweb.controller.AbstractController;
import com.tsystems.logiweb.dto.*;
import com.tsystems.logiweb.dto.converter.Converter;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.facade.OrderFacade;
import com.tsystems.logiweb.facade.impl.OrderFacadeImpl;
import org.apache.log4j.Logger;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Admin on 19.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"MANAGER"}))
@WebServlet(name = "OrderServletController", urlPatterns = {"/employee/orders/*"})
public class OrderController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(OrderController.class);

    private final OrderFacade orderFacade = new OrderFacadeImpl();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ActionURL actionURL = ActionURL.fromUrl(request.getPathInfo());
        switch (actionURL) {
            case DEFAULT:
                handle(() -> getAllOrders(response), response);
                break;
            case DETAILS:
                handle(() -> getOrderDetail(request,response), response);
                break;
            default:
                handle(() -> getOrderByNumber(request, response), response);
                break;
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ActionURL actionURL = ActionURL.fromUrl(request.getPathInfo());
        switch (actionURL) {
            case DEFAULT:
                break;
            case AVAILABLE_TRUCKS:
                handle(() -> availableTrucks(request,response),response);
                break;
            case AVAILABLE_DRIVERS:
                handle(() -> availableDrivers(request,response),response);
                break;
            case SAVE:
                handle(() -> saveOrder(request,response),response);
                break;
        }
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String number = request.getPathInfo().split("/")[1];
        handle(() -> orderFacade.deleteByNumber(Long.valueOf(number)),response);
    }

    private void getOrderDetail(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String orderNumber = request.getPathInfo().split("/")[2];
        OrderDetailDTO orderDetailDTO = new OrderDetailDTO();
        orderDetailDTO.setDrivers(orderFacade.getDriversByOrderNumber(orderNumber));
        List<WaypointDTO> waypoints = orderFacade.getWaypointsByOrderNumber(orderNumber);
        waypoints.sort((w1,w2) -> w1.getOriginalnum() - w2.getOriginalnum());
        orderDetailDTO.setWaypoints(waypoints);
        sendAsJson(response,orderDetailDTO);
    }

    private void getOrderByNumber(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String number = request.getPathInfo().split("/")[1];
        OrderDTO order = orderFacade.getOrderByNumber(Long.valueOf(number));
        sendAsJson(response,order);
    }

    private void availableDrivers(HttpServletRequest request, HttpServletResponse response) throws Exception {
        OrderDTO orderDTO = readObject(request,OrderDTO.class);
        List<DriverDTO> availableDrivers = orderFacade.availableDriversForOrder(orderDTO);
        sendAsJson(response,availableDrivers);
    }

    private void availableTrucks(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<CargoDTO> cargoes = Arrays.asList(readObject(request, CargoDTO[].class));
        List<TruckDTO> availableTrucks = orderFacade.availableTrucksForOrder(cargoes);
        sendAsJson(response,availableTrucks);
    }

    private void saveOrder(HttpServletRequest request, HttpServletResponse response) throws Exception {
        OrderDTO orderDTO = readObject(request,OrderDTO.class);
        orderFacade.insert(orderDTO);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private void getAllOrders(HttpServletResponse response) throws Exception {
        Converter<OrderDTO,OrderInfoDTO> converter = ConverterFactory.createOrderInfoConverter();
        List<OrderInfoDTO> res = new ArrayList<>();
        for(OrderDTO orderDTO : orderFacade.getAll())
            res.add(converter.from(orderDTO));
        sendAsJson(response,res);
    }

}

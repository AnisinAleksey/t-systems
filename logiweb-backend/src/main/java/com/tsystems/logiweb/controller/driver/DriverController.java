package com.tsystems.logiweb.controller.driver;

import com.tsystems.logiweb.controller.AbstractController;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 21.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"DRIVER"}))
@WebServlet(name = "DriverServletController", urlPatterns = {"/driver"})
public class DriverController extends AbstractController {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        handle(() -> request.getRequestDispatcher("/driver/view").forward(request,response), response);
    }
}

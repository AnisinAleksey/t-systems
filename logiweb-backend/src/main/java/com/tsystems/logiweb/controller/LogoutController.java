package com.tsystems.logiweb.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 16.06.2016.
 */
@WebServlet(name = "LogoutController", urlPatterns = {"/logout"})
public class LogoutController extends AbstractController {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handle(() -> processGet(request,response),response);
    }

    private void processGet(HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.logout();
        response.sendRedirect("/");
    }

}

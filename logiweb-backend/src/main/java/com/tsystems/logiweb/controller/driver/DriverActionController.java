package com.tsystems.logiweb.controller.driver;

import com.tsystems.logiweb.controller.AbstractController;
import com.tsystems.logiweb.dto.CargoStatusDTO;
import com.tsystems.logiweb.dto.DriverStatusDTO;
import com.tsystems.logiweb.facade.CargoFacade;
import com.tsystems.logiweb.facade.DriverFacade;
import com.tsystems.logiweb.facade.impl.CargoFacadeImpl;
import com.tsystems.logiweb.facade.impl.DriverFacadeImpl;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 21.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"DRIVER"}))
@WebServlet(name = "DriverActionServletController", urlPatterns = {"/driver/action/*"})
public class DriverActionController extends AbstractController {

    private CargoFacade cargoFacade = new CargoFacadeImpl();
    private DriverFacade driverFacade = new DriverFacadeImpl();

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ActionURL actionURL = ActionURL.fromUrl(request.getPathInfo());
        switch (actionURL) {
            case UPDATE_CARGO_STATUS:
                handle(() -> updateCargoStatus(request, response),response);
                break;
            case CHANGE_DRIVER_STATUS:
                handle(() -> changeStatus(request, response),response);
                break;
        }
    }

    private void updateCargoStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {
        CargoStatusDTO cargoStatusDTO = readObject(request,CargoStatusDTO.class);
        cargoFacade.updateCargoStatus(request.getUserPrincipal().getName(),cargoStatusDTO);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private void changeStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DriverStatusDTO driverStatusDTO = readObject(request,DriverStatusDTO.class);
        driverFacade.changeStatus(Long.valueOf(request.getUserPrincipal().getName()),driverStatusDTO.getStatus());
        response.setStatus(HttpServletResponse.SC_OK);
    }
}

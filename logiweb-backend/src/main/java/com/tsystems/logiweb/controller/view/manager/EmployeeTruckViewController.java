package com.tsystems.logiweb.controller.view.manager;

import com.tsystems.logiweb.controller.view.AbstractViewController;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;

/**
 * Created by Admin on 16.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"MANAGER"}))
@WebServlet(name = "EmployeeTruckViewController", urlPatterns = {"/employee/view/trucks"})
public class EmployeeTruckViewController extends AbstractViewController {

    @Override
    protected String getPath() {
        return "trucks.html";
    }
}

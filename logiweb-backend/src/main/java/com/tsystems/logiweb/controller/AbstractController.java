package com.tsystems.logiweb.controller;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Admin on 15.06.2016.
 */
public abstract class AbstractController extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(AbstractController.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        send404(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        send404(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        send404(request, response);
    }

    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        send404(request, response);
    }

    protected void sendResource(String path, HttpServletRequest request, HttpServletResponse response) throws IOException {
        IOUtils.copy(request.getServletContext().getResourceAsStream(String.join("/","/WEB-INF",path)),response.getOutputStream());
    }

    protected void send404(HttpServletRequest request, HttpServletResponse response) throws IOException {
        sendResource("404.html",request,response);
    }

    protected void send403(HttpServletRequest request, HttpServletResponse response) throws IOException {
        sendResource("403.html",request,response);
    }


    protected void sendAsJson(HttpServletResponse response,Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        response.getOutputStream().print(mapper.writeValueAsString(obj));
    }

    protected <T> T readObject(HttpServletRequest request, Class<T> tClass) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(request.getInputStream(),tClass);
    }

    protected Map<String, String> getQueryParams(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>();
        for (String expression : request.getQueryString().split("&")) {
            params.put(getQueryKey(expression),getQueryValue(expression));
        }
        return params;
    }

    protected void handle(Performable performable,HttpServletResponse response) throws IOException {
        try {
            performable.perform();
        } catch (Exception e) {
            LOG.error(e,e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,e.getMessage());
        }
    }

    private String getQueryKey(String expression) {
        return expression.split("=")[0];
    }

    private String getQueryValue(String expression) {
        String[] entry = expression.split("=");
        return entry.length > 1 ? entry[1] : null;
    }

    protected enum ActionURL {
        SEARCH("/search"), DEFAULT("/"), AVAILABLE_TRUCKS("/availableTrucks"),
        AVAILABLE_DRIVERS("/availableDrivers"), SAVE("/save"), DETAILS("/details"),
        UPDATE_CARGO_STATUS("/updateCargoStatus"), CHANGE_DRIVER_STATUS("/changeDriverStatus");

        private String url;

        ActionURL(String url) {
            this.url = url;
        }

        public static ActionURL fromUrl(String url) {
            String[] partsUrl = url == null ? new String[]{"/"} : url.split("/");
            String firstPart = partsUrl.length > 1 ? partsUrl[1] : partsUrl[0];
            for(ActionURL action : values()) {
                if(Objects.equals("/"+firstPart,action.url))
                    return action;
            }
            return DEFAULT;
        }
    }

    protected interface Performable {
        void perform() throws Exception;
    }



}

package com.tsystems.logiweb.controller.manager;

import com.tsystems.logiweb.controller.AbstractController;
import com.tsystems.logiweb.dto.DriverDTO;
import com.tsystems.logiweb.facade.DriverFacade;
import com.tsystems.logiweb.facade.impl.DriverFacadeImpl;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@ServletSecurity(@HttpConstraint(rolesAllowed={"MANAGER"}))
@WebServlet(name = "DriverServletEmployeeController", urlPatterns = {"/employee/drivers", "/employee/drivers/*"})
public class DriverController extends AbstractController {

    private DriverFacade driverFacade = new DriverFacadeImpl();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> getAllDrivers(response), response);
    }

    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> updateDriver(request, response),response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> createDriver(request, response),response);
    }

    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> deleteDriver(request, response), response);
    }
    private void getAllDrivers(HttpServletResponse response) throws Exception {
        List<DriverDTO> drivers = driverFacade.getAll();
        sendAsJson(response,drivers);
    }

    private void updateDriver(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DriverDTO driverDTO = readObject(request,DriverDTO.class);
        driverFacade.update(driverDTO);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private void createDriver(HttpServletRequest request, HttpServletResponse response) throws Exception {
        DriverDTO driverDTO = readObject(request,DriverDTO.class);
        driverFacade.insert(driverDTO);
        String pass = driverFacade.registerDriver(driverDTO);
        response.setStatus(HttpServletResponse.SC_OK);
        response.getOutputStream().print(pass);
    }

    private void deleteDriver(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id = request.getPathInfo().split("/")[1];
        DriverDTO driverDTO = driverFacade.getDriverByLicence(Long.valueOf(id));
        driverFacade.delete(driverDTO);
        response.setStatus(HttpServletResponse.SC_OK);
    }



}

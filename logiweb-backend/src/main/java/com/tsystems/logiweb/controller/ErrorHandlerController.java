package com.tsystems.logiweb.controller;

import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 14.06.2016.
 */
@WebServlet(name = "ErrorHandlerController", urlPatterns = {"/error"})
public class ErrorHandlerController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(ErrorHandlerController.class);

    @Override
    protected void send404(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Analyze the servlet exception
        Throwable throwable = (Throwable)
                request.getAttribute("javax.servlet.error.exception");
        Integer statusCode = (Integer)
                request.getAttribute("javax.servlet.error.status_code");
        String servletName = (String)
                request.getAttribute("javax.servlet.error.servlet_name");
        LOG.error(String.join(":",servletName,String.valueOf(statusCode)),throwable);
        super.send404(request,response);
    }


}

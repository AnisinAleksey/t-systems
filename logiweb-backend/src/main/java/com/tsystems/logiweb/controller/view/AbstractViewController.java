package com.tsystems.logiweb.controller.view;

import com.tsystems.logiweb.controller.AbstractController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 16.06.2016.
 */
public abstract class AbstractViewController extends AbstractController {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        sendResource(getPath(),request,response);
    }

    protected abstract String getPath();
}

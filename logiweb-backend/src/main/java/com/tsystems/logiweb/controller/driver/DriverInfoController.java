package com.tsystems.logiweb.controller.driver;

import com.tsystems.logiweb.controller.AbstractController;
import com.tsystems.logiweb.dto.DriverDTO;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.facade.DriverFacade;
import com.tsystems.logiweb.facade.impl.DriverFacadeImpl;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@ServletSecurity(@HttpConstraint(rolesAllowed={"DRIVER"}))
@WebServlet(name = "DriverInfoServletController", urlPatterns = {"/driver/driversInfo"})
public class DriverInfoController extends AbstractController {

    private DriverFacade driverFacade = new DriverFacadeImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        handle(() -> getDriverInfo(request,response),response);
    }

    private void getDriverInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Long licence = Long.valueOf(request.getUserPrincipal().getName());
        DriverDTO driverDTO = driverFacade.getDriverByLicence(licence);
        sendAsJson(response,ConverterFactory.createDriverInfoDTOConverter().to(driverDTO));
    }
}

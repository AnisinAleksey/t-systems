package com.tsystems.logiweb.controller.view.driver;

import com.tsystems.logiweb.controller.view.AbstractViewController;

import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;

/**
 * Created by Admin on 21.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"DRIVER"}))
@WebServlet(name = "DriverInfoViewController", urlPatterns = {"/driver/view"})
public class DriverInfoViewController extends AbstractViewController {
    @Override
    protected String getPath() {
        return "driverPage.html";
    }
}

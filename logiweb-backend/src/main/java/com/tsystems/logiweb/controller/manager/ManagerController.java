package com.tsystems.logiweb.controller.manager;

import com.tsystems.logiweb.controller.AbstractController;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Admin on 13.06.2016.
 */
@ServletSecurity(@HttpConstraint(rolesAllowed={"MANAGER"}))
@WebServlet(name = "ManagerServletController", urlPatterns = {"/employee"})
public class ManagerController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        handle(() -> request.getRequestDispatcher("/employee/view/trucks").forward(request,response), response);
    }


}

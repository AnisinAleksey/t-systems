package com.tsystems.logiweb.engine;

import com.tsystems.logiweb.engine.pool.EntityManagerObjectPool;
import org.apache.commons.pool2.ObjectPool;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;

/**
 * Created by Admin on 13.06.2016.
 */
public class PersistenceManager {

    private static final Logger LOG = Logger.getLogger(PersistenceManager.class);

    private ObjectPool<EntityManager> emPool = new EntityManagerObjectPool();

    public static PersistenceManager getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private final static class InstanceHolder {
        private final static PersistenceManager INSTANCE = new PersistenceManager();
    }

    private PersistenceManager() {}

    public void init() {
        emPool = emPool == null ? new EntityManagerObjectPool() : emPool;
    }

    public void destroy() {
        emPool.close();
        emPool = null;
    }

    public EntityManager borrowEntityManager() {
        try {
            return emPool.borrowObject();
        } catch (Exception e) {
            LOG.error("Can't borrow Entity Manager from pool",e);
        }
        return null;
    }

    public void returnEntityManager(EntityManager entityManager) {
        try {
            emPool.returnObject(entityManager);
        } catch (Exception e) {
            LOG.error("Can't return Entity Manager to pool",e);
        }
    }



}

package com.tsystems.logiweb.engine.pool;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Created by Admin on 13.06.2016.
 */
class EntityManagerPooledObjectFactory extends BasePooledObjectFactory<EntityManager> {

    private EntityManagerFactory emf;

    EntityManagerPooledObjectFactory() {

    }

    @Override
    public EntityManager create() throws Exception {
        return emf.createEntityManager();
    }

    @Override
    public PooledObject<EntityManager> wrap(EntityManager entityManager) {
        return new DefaultPooledObject<>(entityManager);
    }

    @Override
    public void destroyObject(PooledObject<EntityManager> p) {
        if(p.getObject().isOpen())
            p.getObject().close();
    }


    public boolean validateObject(PooledObject<EntityManager> p) {
        return p.getObject().isOpen();
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }
}

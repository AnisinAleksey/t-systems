package com.tsystems.logiweb.engine.transaction;

import com.tsystems.logiweb.engine.PersistenceManager;
import com.tsystems.logiweb.engine.transaction.exception.TransactionNotExistException;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;

/**
 * Created by Admin on 17.06.2016.
 */
public class Transaction {

    private static final Logger LOG = Logger.getLogger(Transaction.class);

    private EntityManager em;

    public void begin() {
        em = PersistenceManager.getInstance().borrowEntityManager();
        tryRollback();
        em.getTransaction().begin();
    }

    public void commit() {
        em.getTransaction().commit();
        PersistenceManager.getInstance().returnEntityManager(em);
    }

    public void rollback() {
        tryRollback();
        PersistenceManager.getInstance().returnEntityManager(em);
    }

    private void tryRollback() {
        if(em != null) {
            try {
                em.getTransaction().rollback();
            } catch (Exception e) {
                LOG.warn(e.getMessage());
            }
        }
    }

    public EntityManager getEntityManager() {
        if(em != null) {
            return em;
        }
        throw new TransactionNotExistException();
    }


}

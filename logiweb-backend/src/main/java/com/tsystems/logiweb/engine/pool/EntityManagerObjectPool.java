package com.tsystems.logiweb.engine.pool;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Admin on 13.06.2016.
 */
public class EntityManagerObjectPool extends GenericObjectPool<EntityManager> {

    private static final Logger LOG = Logger.getLogger(EntityManagerObjectPool.class);

    private static final String PERSISTENCE_UNIT = "logiweb";


    private EntityManagerFactory emf;


    public EntityManagerObjectPool() {
        super(new EntityManagerPooledObjectFactory());
        try {
            emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
        } catch (Exception e) {
            LOG.error(e,e);
        }
        ((EntityManagerPooledObjectFactory) getFactory()).setEntityManagerFactory(emf);
    }

    @Override
    public void close() {
        super.close();
        emf.close();
    }
}

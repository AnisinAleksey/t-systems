package com.tsystems.logiweb.engine.transaction;

import javax.persistence.EntityManager;

/**
 * Created by Admin on 17.06.2016.
 */
public interface TransactionCallable<V> {

    V perform(EntityManager em) throws Exception;
}

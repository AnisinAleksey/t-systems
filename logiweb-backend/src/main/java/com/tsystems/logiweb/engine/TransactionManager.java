package com.tsystems.logiweb.engine;

import com.tsystems.logiweb.engine.transaction.Transaction;
import com.tsystems.logiweb.engine.transaction.TransactionCallable;

import javax.persistence.EntityManager;

/**
 * Created by Admin on 17.06.2016.
 */
public class TransactionManager {

    private static ThreadLocal<Transaction> TRANSACTION_HOLDER = new ThreadLocal<Transaction>() {
        @Override
        public Transaction initialValue() {
            return new Transaction();
        }
    };

    public static Transaction getCurrentTransaction() {
        return TRANSACTION_HOLDER.get();
    }

    public static <V> V executeInCurrentTransaction(TransactionCallable<V> executor) throws Exception {
        return execute(getCurrentTransaction(), executor);
    }

    public static <V> V executeInNewTransaction(TransactionCallable<V> executor) throws Exception {
        return execute(new Transaction(),executor);
    }

    public static <V> V executeOutTransaction(TransactionCallable<V> executor) throws Exception {
        return execute(null,executor);
    }

    private static <V> V execute(Transaction transaction, TransactionCallable<V> executor) throws Exception {
        V result;
        if(transaction == null) {
            EntityManager em = PersistenceManager.getInstance().borrowEntityManager();
            result = executor.perform(em);
            PersistenceManager.getInstance().returnEntityManager(em);
            return result;
        }
        try {
            transaction.begin();
            result = executor.perform(transaction.getEntityManager());
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
        return result;
    }

}

package com.tsystems.logiweb.engine.transaction.exception;

/**
 * Created by Admin on 17.06.2016.
 */
public class TransactionNotExistException extends RuntimeException {

    public TransactionNotExistException() {
        super("Transaction has no started yet. Use beginTransaction() before");
    }

}

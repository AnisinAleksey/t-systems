package com.tsystems.logiweb.facade;

import com.tsystems.logiweb.dto.TruckDTO;

import java.util.List;

public interface TruckFacade extends Facade<TruckDTO> {

    List<TruckDTO> trucksForOrder(Long orderNumber) throws Exception;
    TruckDTO getTruckByRegnumber(String regnumber) throws Exception;
}

package com.tsystems.logiweb.facade;


import com.tsystems.logiweb.dto.CargoDTO;
import com.tsystems.logiweb.dto.CargoStatusDTO;

public interface CargoFacade extends Facade<CargoDTO> {

    void updateCargoStatus(String driverLicence,CargoStatusDTO cargoStatusDTO) throws Exception;

}

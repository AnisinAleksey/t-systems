package com.tsystems.logiweb.facade;

import com.tsystems.logiweb.dto.WaypointDTO;

public interface WaypointFacade extends Facade<WaypointDTO> {

}

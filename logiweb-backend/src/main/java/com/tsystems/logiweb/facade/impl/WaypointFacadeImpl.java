package com.tsystems.logiweb.facade.impl;

import com.tsystems.logiweb.dto.WaypointDTO;
import com.tsystems.logiweb.dto.converter.Converter;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.facade.WaypointFacade;
import com.tsystems.logiweb.service.Service;
import com.tsystems.logiweb.service.WaypointService;
import com.tsystems.logiweb.service.impl.WaypointServiceImpl;

public class WaypointFacadeImpl extends AbstractFacade<Waypoint, WaypointDTO> implements WaypointFacade {

    private final WaypointService waypointService = new WaypointServiceImpl();

    @Override
    protected Converter<Waypoint, WaypointDTO> getDefaultConverter() {
        return ConverterFactory.createWaypointConverter();
    }

    @Override
    protected Service<Waypoint> getDefaultService() {
        return waypointService;
    }
}

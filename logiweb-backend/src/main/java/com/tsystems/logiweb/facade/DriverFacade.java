package com.tsystems.logiweb.facade;


import com.tsystems.logiweb.dto.DriverDTO;

/**
 * Created by Алексей on 04.06.2016.
 */
public interface DriverFacade extends Facade<DriverDTO> {
    DriverDTO getDriverByLicence(Long licence) throws Exception;
    String registerDriver(DriverDTO driverDTO) throws Exception;
    void changeStatus(Long number, String status) throws Exception;
}

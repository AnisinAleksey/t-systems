package com.tsystems.logiweb.facade.impl;

import com.tsystems.logiweb.dto.CargoDTO;
import com.tsystems.logiweb.dto.CargoStatusDTO;
import com.tsystems.logiweb.dto.converter.Converter;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.Cargo;
import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.entities.enums.CargoStatus;
import com.tsystems.logiweb.facade.CargoFacade;
import com.tsystems.logiweb.service.CargoService;
import com.tsystems.logiweb.service.DriverService;
import com.tsystems.logiweb.service.OrderService;
import com.tsystems.logiweb.service.Service;
import com.tsystems.logiweb.service.impl.CargoServiceImpl;
import com.tsystems.logiweb.service.impl.DriverServiceImpl;
import com.tsystems.logiweb.service.impl.OrderServiceImpl;

public class CargoFacadeImpl extends AbstractFacade<Cargo,CargoDTO> implements CargoFacade {

    private final CargoService cargoService = new CargoServiceImpl();
    private final DriverService driverService = new DriverServiceImpl();
    private final OrderService orderService = new OrderServiceImpl();

    @Override
    protected Converter<Cargo, CargoDTO> getDefaultConverter() {
        return ConverterFactory.createCargoConverter();
    }

    @Override
    protected Service<Cargo> getDefaultService() {
        return cargoService;
    }

    @Override
    public void updateCargoStatus(String driverLicence,CargoStatusDTO cargoStatusDTO) throws Exception {
        Driver driver = driverService.getDriverByLicence(Long.valueOf(driverLicence));
        Cargo cargo = cargoService.getCargoByNumber(Long.valueOf(cargoStatusDTO.getNumber()));
        cargoService.updateCargo(driver, cargo, CargoStatus.valueOf(cargoStatusDTO.getStatus()));
    }
}

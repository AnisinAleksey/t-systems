package com.tsystems.logiweb.facade;

import com.tsystems.logiweb.dto.CityDTO;

import java.util.List;

public interface CityFacade extends Facade<CityDTO> {

    List<CityDTO> searchByName(String likeName) throws Exception;

    List<CityDTO> searchByName(String likeName, int limit) throws Exception;

    CityDTO getCityByName(String name) throws Exception;

}

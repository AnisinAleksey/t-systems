package com.tsystems.logiweb.facade.impl;

import com.tsystems.logiweb.dto.DriverDTO;
import com.tsystems.logiweb.dto.converter.Converter;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.entities.enums.DriverStatus;
import com.tsystems.logiweb.facade.DriverFacade;
import com.tsystems.logiweb.service.DriverService;
import com.tsystems.logiweb.service.Service;
import com.tsystems.logiweb.service.UserService;
import com.tsystems.logiweb.service.impl.DriverServiceImpl;
import com.tsystems.logiweb.service.impl.UserServiceImpl;

/**
 * Created by Алексей on 04.06.2016.
 */
public class DriverFacadeImpl extends AbstractFacade<Driver,DriverDTO> implements DriverFacade {

    private final DriverService driverService = new DriverServiceImpl();
    private final UserService userService = new UserServiceImpl();

    @Override
    public DriverDTO getDriverByLicence(Long licence) throws Exception {
        return getDefaultConverter().from(driverService.getDriverByLicence(licence));
    }

    @Override
    public String registerDriver(DriverDTO driverDTO) throws Exception {
        return userService.registerDriver(driverDTO.getP_number().toString());
    }

    @Override
    public void changeStatus(Long number, String status) throws Exception {
        driverService.changeStatus(driverService.getDriverByLicence(number), DriverStatus.valueOf(status));
    }

    @Override
    protected Converter<Driver, DriverDTO> getDefaultConverter() {
        return ConverterFactory.createDriverConverter();
    }

    @Override
    protected Service<Driver> getDefaultService() {
        return driverService;
    }

}


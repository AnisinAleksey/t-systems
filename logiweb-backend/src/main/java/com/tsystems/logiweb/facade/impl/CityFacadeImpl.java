package com.tsystems.logiweb.facade.impl;

import com.tsystems.logiweb.dto.CityDTO;
import com.tsystems.logiweb.dto.converter.Converter;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.facade.CityFacade;
import com.tsystems.logiweb.service.CityService;
import com.tsystems.logiweb.service.Service;
import com.tsystems.logiweb.service.impl.CityServiceImpl;

import java.util.List;

public class CityFacadeImpl extends AbstractFacade<City, CityDTO> implements CityFacade {

    private final CityService cityService = new CityServiceImpl();

    @Override
    public List<CityDTO> searchByName(String likeName) throws Exception {
        return this.searchByName(likeName, 5);
    }

    @Override
    public List<CityDTO> searchByName(String likeName, int limit) throws Exception {
        return convertList(cityService.searchByName(likeName, limit));
    }

    @Override
    public CityDTO getCityByName(String name) throws Exception {
        return getDefaultConverter().from(cityService.getCityByName(name));
    }

    @Override
    protected Converter<City, CityDTO> getDefaultConverter() {
        return ConverterFactory.createCityConverter();
    }

    @Override
    protected Service<City> getDefaultService() {
        return cityService;
    }

}

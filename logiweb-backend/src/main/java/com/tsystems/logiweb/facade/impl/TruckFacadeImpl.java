package com.tsystems.logiweb.facade.impl;

import com.tsystems.logiweb.dto.TruckDTO;
import com.tsystems.logiweb.dto.converter.Converter;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.facade.TruckFacade;
import com.tsystems.logiweb.service.OrderService;
import com.tsystems.logiweb.service.Service;
import com.tsystems.logiweb.service.TruckService;
import com.tsystems.logiweb.service.impl.OrderServiceImpl;
import com.tsystems.logiweb.service.impl.TruckServiceImpl;

import java.util.Collections;
import java.util.List;

public class TruckFacadeImpl extends AbstractFacade<Truck,TruckDTO> implements TruckFacade {

    private final TruckService truckService = new TruckServiceImpl();
    private final OrderService orderService = new OrderServiceImpl();

    @Override
    protected Converter<Truck, TruckDTO> getDefaultConverter() {
        return ConverterFactory.createTruckConverter();
    }

    @Override
    protected Service<Truck> getDefaultService() {
        return truckService;
    }

    @Override
    public List<TruckDTO> trucksForOrder(Long orderNumber) throws Exception {
        Order order = orderService.getOrderByNumber(orderNumber);
        return Collections.emptyList();
    }

    public TruckDTO getTruckByRegnumber(String regnumber) throws Exception {
        return getDefaultConverter().from(truckService.getTruckByRegnumber(regnumber));
    }
}

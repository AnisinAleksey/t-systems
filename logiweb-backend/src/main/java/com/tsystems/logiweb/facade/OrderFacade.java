package com.tsystems.logiweb.facade;


import com.tsystems.logiweb.dto.*;

import java.util.List;

public interface OrderFacade extends Facade<OrderDTO> {
    OrderDTO getOrderByNumber(Long number) throws Exception;
    List<TruckDTO> availableTrucksForOrder(List<CargoDTO> cargoes) throws Exception;
    List<DriverDTO> availableDriversForOrder(OrderDTO orderDTO) throws Exception;
    List<DriverDTO> getDriversByOrderNumber(String number) throws Exception;
    List<WaypointDTO> getWaypointsByOrderNumber(String number) throws Exception;
    void deleteByNumber(Long number) throws Exception;
}

package com.tsystems.logiweb.facade.impl;

import com.tsystems.logiweb.dto.*;
import com.tsystems.logiweb.dto.converter.Converter;
import com.tsystems.logiweb.dto.converter.factory.ConverterFactory;
import com.tsystems.logiweb.entities.*;
import com.tsystems.logiweb.facade.OrderFacade;
import com.tsystems.logiweb.service.DriverService;
import com.tsystems.logiweb.service.OrderService;
import com.tsystems.logiweb.service.Service;
import com.tsystems.logiweb.service.TruckService;
import com.tsystems.logiweb.service.impl.DriverServiceImpl;
import com.tsystems.logiweb.service.impl.OrderServiceImpl;
import com.tsystems.logiweb.service.impl.TruckServiceImpl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class OrderFacadeImpl extends AbstractFacade<Order, OrderDTO> implements OrderFacade {

    private OrderService orderService = new OrderServiceImpl();
    private TruckService truckService = new TruckServiceImpl();
    private DriverService driverService = new DriverServiceImpl();

    @Override
    protected Converter<Order, OrderDTO> getDefaultConverter() {
        return ConverterFactory.createOrderConverter();
    }

    @Override
    protected Service<Order> getDefaultService() {
        return orderService;
    }

    @Override
    public OrderDTO getOrderByNumber(Long number) throws Exception {
        return getDefaultConverter().from(orderService.getOrderByNumber(number));
    }

    @Override
    public void insert(OrderDTO orderDTO) throws Exception {
        Order order = ConverterFactory.createOrderConverter().to(orderDTO);
        Converter<Waypoint,WaypointDTO> waypointConverter = ConverterFactory.createWaypointConverter();
        List<Waypoint> waypoints = new LinkedList<>();
        for(WaypointDTO waypointDTO : orderDTO.getWaypoints()) {
            waypoints.add(waypointConverter.to(waypointDTO));
        }
        Converter<Cargo,CargoDTO> cargoConverter = ConverterFactory.createCargoConverter();
        List<Cargo> cargoes = new LinkedList<>();
        for(CargoDTO cargoDTO : orderDTO.getCargoes()) {
            cargoes.add(cargoConverter.to(cargoDTO));
        }
        Truck truck = truckService.getTruckByRegnumber(orderDTO.getTruck());
        List<Driver> drivers = new LinkedList<>();
        for(String driver : orderDTO.getDrivers()) {
            drivers.add(driverService.getDriverByLicence(Long.valueOf(driver)));
        }
        orderService.saveOrder(order, waypoints, cargoes, drivers, truck);
    }

    @Override
    public List<TruckDTO> availableTrucksForOrder(List<CargoDTO> cargoes) throws Exception {
        Converter<Cargo,CargoDTO> cargoConverter = ConverterFactory.createCargoConverter();
        List<Cargo> cargoEntities = new LinkedList<>();
        for(CargoDTO cargoDTO : cargoes) {
            cargoEntities.add(cargoConverter.to(cargoDTO));
        }
        List<Truck> trucks = truckService.availableTrucks(cargoEntities);
        List<TruckDTO> trucksDTO = new LinkedList<>();
        Converter<Truck,TruckDTO> truckConverter = ConverterFactory.createTruckConverter();
        for(Truck truck : trucks) {
            trucksDTO.add(truckConverter.from(truck));
        }
        return trucksDTO;
    }

    @Override
    public List<DriverDTO> availableDriversForOrder(OrderDTO orderDTO) throws Exception {
        List<DriverDTO> availableDrivers = new LinkedList<>();
        List<WaypointDTO> waypointsDTO = orderDTO.getWaypoints();
        List<Waypoint> waypoints = new LinkedList<>();
        Converter<Waypoint,WaypointDTO> waypointConverter = ConverterFactory.createWaypointConverter();
        for(WaypointDTO waypointDTO : waypointsDTO) {
            waypoints.add(waypointConverter.to(waypointDTO));
        }
        List<Driver> drivers = driverService.getSuitableDriversForOrder(truckService.getTruckByRegnumber(orderDTO.getTruck()),waypoints);
        Converter<Driver,DriverDTO> driverConverter = ConverterFactory.createDriverConverter();
        for(Driver driver : drivers) {
            availableDrivers.add(driverConverter.from(driver));
        }
        return availableDrivers;
    }

    @Override
    public List<DriverDTO> getDriversByOrderNumber(String number) throws Exception {
        Converter<Driver, DriverDTO> driverConverter = ConverterFactory.createDriverConverter();
        return orderService.getOrderByNumber(Long.valueOf(number)).getDrivers()
                .stream().map(driverConverter::from).collect(Collectors.toList());
    }

    @Override
    public List<WaypointDTO> getWaypointsByOrderNumber(String number) throws Exception {
        Converter<Waypoint, WaypointDTO> waypointConverter = ConverterFactory.createWaypointConverter();
        return orderService.getOrderByNumber(Long.valueOf(number)).getWaypoints()
                .stream().map(waypointConverter::from).collect(Collectors.toList());
    }

    @Override
    public void deleteByNumber(Long number) throws Exception {
        orderService.deleteByNumber(number);
    }
}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.JpaDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.entities.Cargo;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.service.TruckService;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

public class TruckServiceImpl extends AbstractService<Truck> implements TruckService {

    @Override
    public List<Truck> availableTrucks(List<Cargo> cargoes) throws Exception {
        return TransactionManager.executeOutTransaction(em -> {
            List<Truck> trucks = FactoryDao.createTruckDao(em).truckInAvailableCondition()
                    .stream().filter(truck -> Objects.isNull(truck.getOrder())).collect(Collectors.toList());
            List<Truck> filterTrucks = new LinkedList<>();
            for (Truck availableTruck : trucks) {
                if(checkAvailableTruck(availableTruck, cargoes))
                    filterTrucks.add(availableTruck);
            }
            return filterTrucks;
        });
    }

    private boolean checkAvailableTruck(Truck truck, List<Cargo> cargoes) {
        Map<Integer, Double> capacity = new HashMap<>();
        for(Cargo cargo : cargoes) {
            Double load = capacity.get(cargo.getWaypointLoad().getOriginalNumber());
            capacity.put(cargo.getWaypointLoad().getOriginalNumber(), cargo.getWeight() + (load == null ? 0 : load));
            Double unload = capacity.get(cargo.getWaypointUnload().getOriginalNumber());
            capacity.put(cargo.getWaypointUnload().getOriginalNumber(), (unload == null ? 0 : unload) - cargo.getWeight());
        }
        Double point = null;
        for(Double d : capacity.values()) {
            if(point == null)
                point = d;
            else
                point += d;
            if(truck.getCapacity() < point)
                return false;
        }
        return true;
    }

    @Override
    public Truck refreshOrder(Truck truck) throws Exception {
        Truck t = getTruckByRegnumber(truck.getRegnumber());
        truck.setOrder(t == null ? null : t.getOrder());
        return truck;
    }

    public Truck getTruckByRegnumber(String regnumber) throws Exception {
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createTruckDao(em).getTruckByRegnumber(regnumber));
    }

    @Override
    public Truck refreshId(Truck entity) throws Exception {
        Truck t = getTruckByRegnumber(entity.getRegnumber());
        entity.setId(t == null ? null : t.getId());
        return entity;
    }

    @Override
    protected JpaDao<Truck> getDefaultDao(EntityManager em) {
        return FactoryDao.createTruckDao(em);
    }
}

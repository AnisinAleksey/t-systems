package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.JpaDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.service.WaypointService;

import javax.persistence.EntityManager;
import java.util.List;

public class WaypointServiceImpl extends AbstractService<Waypoint> implements WaypointService{

    @Override
    public Waypoint refreshId(Waypoint entity) throws Exception {
        if(entity.getCity() == null || entity.getOrder() == null)
            return entity;
        Waypoint w = TransactionManager.executeOutTransaction(em -> FactoryDao.createWaypointDao(em)
                .findWaypointByOrderCity(entity.getOrder(), entity.getCity()));
        entity.setId(w == null ? null : w.getId());
        return entity;
    }

    @Override
    public List<Waypoint> getListOfWaypoint(Order order) throws Exception {
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createWaypointDao(em).getListOfWaypoint(order));
    }

    @Override
    protected JpaDao<Waypoint> getDefaultDao(EntityManager em) {
        return FactoryDao.createWaypointDao(em);
    }
}

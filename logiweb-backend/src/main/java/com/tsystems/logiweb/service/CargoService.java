package com.tsystems.logiweb.service;


import com.tsystems.logiweb.entities.Cargo;
import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.entities.enums.CargoStatus;

public interface CargoService extends Service<Cargo> {
    Cargo getCargoByNumber(Long num) throws Exception;

    void updateCargo(Driver driver, Cargo cargo, CargoStatus cargoStatus) throws Exception;
}

package com.tsystems.logiweb.service;


public interface UserService {
    /**
     *
     * @param personalNumber - personal Driver number
     * @return generated password for Driver
     * @throws Exception
     */
    String registerDriver(String personalNumber) throws Exception;
}

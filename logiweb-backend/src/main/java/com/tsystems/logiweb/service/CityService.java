package com.tsystems.logiweb.service;

import com.tsystems.logiweb.entities.City;

import java.util.List;

public interface CityService extends Service<City> {

    List<City> searchByName(String likeName) throws Exception;

    List<City> searchByName(String likeName, int limit) throws Exception;

    City getCityByName(String name) throws Exception;

}

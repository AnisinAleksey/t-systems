package com.tsystems.logiweb.service;

import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Waypoint;

import java.util.List;

public interface WaypointService extends Service<Waypoint> {
    List<Waypoint> getListOfWaypoint(Order order) throws Exception;
}

package com.tsystems.logiweb.service;


import com.tsystems.logiweb.entities.*;

import java.util.List;

public interface OrderService extends Service<Order> {
    Order getOrderByNumber(Long number) throws Exception;
    Order refreshTruck(Order order) throws Exception;
    Order refreshWaypoints(Order order) throws Exception;
    void saveOrder(Order order, List<Waypoint> waypoints, List<Cargo> cargoes, List<Driver> drivers, Truck truck) throws Exception;
    boolean isMade(Order order) throws Exception;
    void deleteByNumber(Long number) throws Exception;
}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.JpaDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.entities.enums.DriverStatus;
import com.tsystems.logiweb.exception.DriverHasOrderException;
import com.tsystems.logiweb.service.DriverService;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class DriverServiceImpl extends AbstractService<Driver> implements DriverService {

    @Override
    public Driver getDriverByLicence(long licence) throws Exception {
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createDriverDao(em).getDriverByLicence(licence));
    }

    @Override
    public List<Long> getCoDriversByLicence(Long licence) throws Exception {
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createDriverDao(em).getCoDriversByLicence(licence));
    }

    @Override
    public List<Driver> getSuitableDriversForOrder(Truck truck, Collection<Waypoint> waypoints) throws Exception {

        return TransactionManager.executeOutTransaction(em -> {
            List<Driver> availableDrivers = FactoryDao.createDriverDao(em).freeDrivers();
            availableDrivers = availableDrivers.stream()
                    .filter(driver -> Objects.equals(driver.getCity().getName(), truck.getCity().getName()))
                    .filter(driver -> availableDriver(driver,waypoints))
                    .collect(Collectors.toList());
            return availableDrivers;
        });
    }

    private boolean availableDriver(Driver driver, Collection<Waypoint> waypoints) {
        double wayHours = 0;
        Waypoint previousWaypoint = null;
        for(Waypoint waypoint : waypoints) {
            if(previousWaypoint == null) {
                previousWaypoint = waypoint;
                continue;
            }
            wayHours += distanceBetweenTwoCities(previousWaypoint.getCity(),waypoint.getCity()) / 60;
        }
        return driver.getHours() + wayHours <= 176;
    }

    public double distanceBetweenTwoCities(City city1, City city2) {
        return  111.2 * Math.sqrt( (city1.getLongitudine() - city2.getLongitudine())*(city1.getLongitudine() - city2.getLongitudine()) + (city1.getLatitudine() - city2.getLatitudine())*Math.cos(Math.PI*city1.getLongitudine()/180)*(city1.getLatitudine() - city2.getLatitudine())*Math.cos(Math.PI*city1.getLongitudine()/180));
    }

    @Override
    public Driver refreshTruck(Driver driver) throws Exception {
        Driver d = getDriverByLicence(driver.getP_number());
        driver.setTruck(d == null ? null : d.getTruck());
        return driver;
    }

    @Override
    public Driver refreshOrder(Driver driver) throws Exception {
        Driver d = getDriverByLicence(driver.getP_number());
        driver.setOrder(d == null ? null : d.getOrder());
        return driver;
    }

    @Override
    public void changeStatus(Driver driver, DriverStatus driverStatus) throws Exception {
        TransactionManager.executeInCurrentTransaction((em) -> {
            if(driverStatus == DriverStatus.REST) {
                if(driver.getOrder() != null)
                    throw new DriverHasOrderException();
                driver.setTruck(null);
            }
            driver.setStatus(driverStatus);
            FactoryDao.createDriverDao(em).update(driver);
            return null;
        });
    }

    @Override
    public Driver refreshId(Driver entity) throws Exception {
        Driver d = getDriverByLicence(entity.getP_number());
        entity.setId(d == null ? null : d.getId());
        return entity;
    }


    @Override
    protected JpaDao<Driver> getDefaultDao(EntityManager em) {
        return FactoryDao.createDriverDao(em);
    }
}


package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.*;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.entities.*;
import com.tsystems.logiweb.entities.enums.CargoStatus;
import com.tsystems.logiweb.entities.enums.DriverStatus;
import com.tsystems.logiweb.service.*;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

public class OrderServiceImpl extends AbstractService<Order> implements OrderService {

    @Override
    public Order refreshId(Order entity) throws Exception {
        Order o = getOrderByNumber(entity.getNumber());
        entity.setId(o == null ? null : o.getId());
        return entity;
    }

    @Override
    public Order getOrderByNumber(Long number) throws Exception {
        if(number == null)
            return null;
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createOrderDao(em).getOrderByNumber(number));
    }

    @Override
    public Order refreshTruck(Order order) throws Exception {
        Order o = getOrderByNumber(order.getNumber());
        order.setTruck(o == null ? null : o.getTruck());
        return order;
    }

    @Override
    public Order refreshWaypoints(Order order) throws Exception {
        Order o = getOrderByNumber(order.getNumber());
        order.setWaypoints(o == null ? null : o.getWaypoints());
        return order;
    }


    @Override
    public void saveOrder(Order order, List<Waypoint> waypoints, List<Cargo> cargoes, List<Driver> drivers, Truck truck) throws Exception {
        TransactionManager.executeInCurrentTransaction((em) -> {
            WaypointDao waypointDao = FactoryDao.createWaypointDao(em);
            CargoDao cargoDao = FactoryDao.createCargoDao(em);
            OrderDao orderDao = FactoryDao.createOrderDao(em);
            DriverDao driverDao = FactoryDao.createDriverDao(em);
            TruckDao truckDao = FactoryDao.createTruckDao(em);
            orderDao.insert(order);
            for (Waypoint waypoint : waypoints) {
                waypoint.setOrder(order);
                waypointDao.insert(waypoint);
            }
            for (Cargo cargo : cargoes) {
                cargo.setWaypointLoad(waypoints
                        .stream().filter((w) -> Objects.equals(cargo.getWaypointLoad().getCity(), w.getCity()) &&
                                Objects.equals(cargo.getWaypointLoad().getOriginalNumber(), w.getOriginalNumber()))
                        .findFirst().get());
                cargo.setWaypointUnload(waypoints
                        .stream().filter((w) -> Objects.equals(cargo.getWaypointUnload().getCity(), w.getCity()) &&
                                Objects.equals(cargo.getWaypointUnload().getOriginalNumber(), w.getOriginalNumber()))
                        .findFirst().get());
                cargo.setStatus(CargoStatus.PREPARED);
                cargo.setOrder(order);
                cargoDao.insert(cargo);
            }
            truck.setOrder(order);
            truckDao.update(truck);
            for (Driver driver : drivers) {
                driver.setOrder(order);
                driver.setTruck(truck);
                driver.setStatus(DriverStatus.BUSY);
                driverDao.update(driver);
            }
            order.setTruck(truck);
            order.setDrivers(drivers);
            order.setWaypoints(waypoints);
            order.setCargoes(cargoes);
            orderDao.update(order);
            return null;
        });
    }

    @Override
    public boolean isMade(Order order) throws Exception {
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createOrderDao(em).isMade(order));
    }

    @Override
    public void deleteByNumber(Long number) throws Exception {
        TransactionManager.executeInCurrentTransaction((em) -> {
            OrderDao orderDao = FactoryDao.createOrderDao(em);
            DriverDao driverDao = FactoryDao.createDriverDao(em);
            TruckDao truckDao = FactoryDao.createTruckDao(em);
            CargoDao cargoDao = FactoryDao.createCargoDao(em);
            WaypointDao waypointDao = FactoryDao.createWaypointDao(em);
            Order order = orderDao.getOrderByNumber(number);
            if(order.getDrivers() != null) {
                for(Driver driver : order.getDrivers()) {
                    driver.setOrder(null);
                    driver.setStatus(DriverStatus.REST);
                    driver.setTruck(null);
                    driverDao.update(driver);
                }
            }
            if(order.getTruck() != null) {
                Truck truck = order.getTruck();
                truck.setOrder(null);
                truckDao.update(truck);
            }
            order.getCargoes().forEach(cargoDao::delete);
            order.getWaypoints().forEach(waypointDao::delete);
            orderDao.delete(order);
            return null;
        });
    }

    @Override
    protected JpaDao<Order> getDefaultDao(EntityManager em) {
        return FactoryDao.createOrderDao(em);
    }
}

package com.tsystems.logiweb.service;


import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.entities.enums.DriverStatus;

import java.util.Collection;
import java.util.List;


public interface DriverService extends Service<Driver>{
    Driver getDriverByLicence(long licence) throws Exception;
    List<Long> getCoDriversByLicence(Long licence) throws Exception;
    List<Driver> getSuitableDriversForOrder(Truck truck, Collection<Waypoint> waypoints) throws Exception;
    Driver refreshTruck(Driver driver) throws Exception;
    Driver refreshOrder(Driver driver) throws Exception;
    void changeStatus(Driver driverByLicence, DriverStatus driverStatus) throws Exception;
}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.DriverDao;
import com.tsystems.logiweb.dao.JpaDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.entities.Cargo;
import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.entities.enums.CargoStatus;
import com.tsystems.logiweb.entities.enums.DriverStatus;
import com.tsystems.logiweb.exception.UnavailableCargoModificationException;
import com.tsystems.logiweb.service.CargoService;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

public class CargoServiceImpl extends AbstractService<Cargo> implements CargoService {

    @Override
    public Cargo refreshId(Cargo entity) throws Exception {
        if(entity.getNum() == null)
            return entity;
        Cargo c = getCargoByNumber(entity.getNum());
        entity.setId(c == null ? null : c.getId());
        return entity;
    }

    @Override
    public Cargo getCargoByNumber(Long num) throws Exception {
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createCargoDao(em).findCargoByNumber(num));
    }

    @Override
    public void updateCargo(Driver driver, Cargo cargo, CargoStatus cargoStatus) throws Exception {
        TransactionManager.executeInCurrentTransaction((em) -> {
            Order order = cargo.getOrder();
            if (!Objects.equals(order.getNumber(), driver.getOrder().getNumber()))
                throw new UnavailableCargoModificationException(cargo.getNum().toString());
            cargo.setStatus(cargoStatus);
            FactoryDao.createCargoDao(em).update(cargo);
            List<Cargo> cargos = order.getCargoes();
            if(cargos.stream().allMatch(c -> c.getStatus() == CargoStatus.DELIVERED)) {
                order.setState(true);
                Truck truck = order.getTruck();
                truck.setOrder(null);
                FactoryDao.createTruckDao(em).update(truck);
                order.setTruck(null);
                DriverDao driverDao = FactoryDao.createDriverDao(em);
                for(Driver d: order.getDrivers()) {
                    d.setOrder(null);
                    d.setTruck(null);
                    d.setStatus(DriverStatus.REST);
                    driverDao.update(d);
                }
                order.getDrivers().clear();
                FactoryDao.createOrderDao(em).update(order);
            }
            return null;
        });
    }

    @Override
    protected JpaDao<Cargo> getDefaultDao(EntityManager em) {
        return FactoryDao.createCargoDao(em);
    }
}

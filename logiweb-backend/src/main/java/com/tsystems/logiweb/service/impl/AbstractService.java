package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.JpaDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.service.Service;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractService<T> implements Service<T> {

    @Override
    public List<T> getAll() throws Exception {
        return TransactionManager.executeOutTransaction(em -> getDefaultDao(em).getAll());
    }

    @Override
    public void delete(T entity) throws Exception {
        TransactionManager.executeInCurrentTransaction(em -> getDefaultDao(em).delete(entity));
    }

    @Override
    public void update(T entity) throws Exception {
        TransactionManager.executeInCurrentTransaction(em -> getDefaultDao(em).update(entity));
    }

    @Override
    public void insert(T entity) throws Exception {
        TransactionManager.executeInCurrentTransaction(em -> getDefaultDao(em).insert(entity));
    }

    @Override
    public T get(Long id) throws Exception {
        return TransactionManager.executeInCurrentTransaction(em -> getDefaultDao(em).get(id));
    }

    protected abstract JpaDao<T> getDefaultDao(EntityManager em);
}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.JpaDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.service.CityService;

import javax.persistence.EntityManager;
import java.util.List;

public class CityServiceImpl extends AbstractService<City> implements CityService{

    @Override
    public List<City> searchByName(String likeName) throws Exception {
        return this.searchByName(likeName,5);
    }

    @Override
    public List<City> searchByName(String likeName, int limit) throws Exception {
            return TransactionManager.executeOutTransaction(em -> FactoryDao.createCityDao(em).searchByName(likeName,limit));

    }

    @Override
    public City getCityByName(String name) throws Exception {
        return TransactionManager.executeOutTransaction(em -> FactoryDao.createCityDao(em).getCityByName(name));
    }

    @Override
    public City refreshId(City entity) throws Exception {
        City c = getCityByName(entity.getName());
        entity.setId(c == null ? null : c.getId());
        return entity;
    }

    @Override
    protected JpaDao<City> getDefaultDao(EntityManager em) {
        return FactoryDao.createCityDao(em);
    }
}

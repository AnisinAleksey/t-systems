package com.tsystems.logiweb.service;

import com.tsystems.logiweb.entities.Cargo;
import com.tsystems.logiweb.entities.Truck;

import java.util.List;

public interface TruckService extends Service<Truck> {

    List<Truck> availableTrucks(List<Cargo> cargoes) throws Exception;
    Truck refreshOrder(Truck truck) throws Exception;
    Truck getTruckByRegnumber(String regnumber) throws Exception;
}

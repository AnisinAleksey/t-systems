package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.TransactionManager;
import com.tsystems.logiweb.service.UserService;
import com.tsystems.logiweb.util.StringUtil;

public class UserServiceImpl implements UserService {

    private static final String DRIVER_ROLE = "DRIVER";

    @Override
    public String registerDriver(String personalNumber) throws Exception {
        return TransactionManager.executeInCurrentTransaction((em) -> FactoryDao.createUserDao(em)
                .registerUser(personalNumber,StringUtil.generateRandomString(), DRIVER_ROLE));
    }
}

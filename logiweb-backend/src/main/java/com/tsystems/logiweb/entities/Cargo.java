package com.tsystems.logiweb.entities;

import com.tsystems.logiweb.entities.enums.CargoStatus;

import javax.persistence.*;

@Entity
@Table(name = "cargo")
public class Cargo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "num")
    private Long num;

    @Column(name = "name")
    private String name;

    @Column(name ="weight")
    private Double weight;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CargoStatus status;

    @OneToOne
    @JoinColumn(name = "waypoint_load_id")
    private Waypoint waypointLoad;

    @OneToOne
    @JoinColumn(name = "waypoint_unload_id")
    private Waypoint waypointUnload;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public CargoStatus getStatus() {
        return status;
    }

    public void setStatus(CargoStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "id=" + id + ", num='" + num + ", weight='" + weight + ", name='" + name;
    }


    public Waypoint getWaypointLoad() {
        return waypointLoad;
    }

    public void setWaypointLoad(Waypoint waypointLoad) {
        this.waypointLoad = waypointLoad;
    }

    public Waypoint getWaypointUnload() {
        return waypointUnload;
    }

    public void setWaypointUnload(Waypoint waypointUnload) {
        this.waypointUnload = waypointUnload;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}

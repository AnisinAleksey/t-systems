package com.tsystems.logiweb.entities.enums;

public enum DriverStatus{
    REST, BUSY, DRIVING;
}

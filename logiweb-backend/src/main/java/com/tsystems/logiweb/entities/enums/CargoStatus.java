package com.tsystems.logiweb.entities.enums;

public enum CargoStatus{
    PREPARED,SHIPPED,DELIVERED;
}
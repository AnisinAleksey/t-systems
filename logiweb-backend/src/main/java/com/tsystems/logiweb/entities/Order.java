package com.tsystems.logiweb.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "`order`")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long number;

    @Column(name = "state")
    private boolean state;

    @OneToOne(mappedBy = "order", fetch = FetchType.LAZY)
    private Truck truck;

    @OneToMany(mappedBy = "order",fetch = FetchType.LAZY)
    @OrderBy("originalNumber ASC")
    private List<Waypoint> waypoints;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private List<Driver> drivers;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY)
    private List<Cargo> cargoes;


    public Order() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public boolean isState() {
        return state;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    @Override
    public String toString() {
        return String.valueOf(number);
    }

    public List<Waypoint> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<Waypoint> waypoints) {
        this.waypoints = waypoints;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    public List<Cargo> getCargoes() {
        return cargoes;
    }

    public void setCargoes(List<Cargo> cargoes) {
        this.cargoes = cargoes;
    }
}

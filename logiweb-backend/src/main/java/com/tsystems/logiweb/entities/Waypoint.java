package com.tsystems.logiweb.entities;

import javax.persistence.*;

@Entity
@Table(name = "waypoint")
public class Waypoint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "city_id")
    private City city;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    @Column(name = "originalnum")
    private Integer originalNumber;


    public Integer getOriginalNumber() {
        return originalNumber;
    }

    public void setOriginalNumber(Integer originalNumber) {
        this.originalNumber = originalNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return String.valueOf(id);
    }


}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.CargoDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.PersistenceManager;
import com.tsystems.logiweb.entities.Cargo;
import com.tsystems.logiweb.service.CargoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FactoryDao.class,PersistenceManager.class})
public class CargoServiceTest {

    private CargoDao cargoDao;
    private CargoService cargoService = new CargoServiceImpl();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(FactoryDao.class);
        PowerMockito.mockStatic(PersistenceManager.class);
        PersistenceManager persistenceManager = mock(PersistenceManager.class);
        PowerMockito.when(PersistenceManager.getInstance()).thenReturn(persistenceManager);
        EntityManager em = mock(EntityManager.class);
        cargoDao = mock(CargoDao.class);
        when(persistenceManager.borrowEntityManager()).thenReturn(em);
        PowerMockito.when(FactoryDao.createCargoDao(em)).thenReturn(cargoDao);
    }

    @Test
    public void getCargoByNumberTest() throws Exception {
        Long number = 35287453l;
        Long id = 23l;
        Cargo cargo = new Cargo();
        cargo.setId(id);
        when(cargoDao.findCargoByNumber(number)).thenReturn(cargo);
        Cargo c = cargoService.getCargoByNumber(number);
        assertNotNull(c);
        assertEquals(id,c.getId());
    }
}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.CityDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.PersistenceManager;
import com.tsystems.logiweb.entities.City;
import com.tsystems.logiweb.service.CityService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FactoryDao.class,PersistenceManager.class})
public class CityServiceTest {

    private CityDao cityDao;
    private CityService cityService = new CityServiceImpl();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(FactoryDao.class);
        PowerMockito.mockStatic(PersistenceManager.class);
        PersistenceManager persistenceManager = mock(PersistenceManager.class);
        PowerMockito.when(PersistenceManager.getInstance()).thenReturn(persistenceManager);
        EntityManager em = mock(EntityManager.class);
        cityDao = mock(CityDao.class);
        when(persistenceManager.borrowEntityManager()).thenReturn(em);
        PowerMockito.when(FactoryDao.createCityDao(em)).thenReturn(cityDao);
    }

    @Test
     public void getCityByNameTest() throws Exception {
         String name = "Pinsk";
         Long id = 89l;
         City city = new City();
         city.setId(id);
         when(cityDao.getCityByName(name)).thenReturn(city);
         City c = cityService.getCityByName(name);
         assertNotNull(c);
         assertEquals(id,c.getId());
     }

    @Test
    public void searchByNameTest() throws Exception {
        String name = "Pinsk";
        String name2 = "Pinsk2";
        Long id = 89l;
        Long id2 = 455l;
        int limit = 2;
        City city = new City();
        city.setId(id);
        City city2 = new City();
        city2.setId(id2);
        List<City> cityList = new ArrayList<>();
        cityList.add(city);
        cityList.add(city2);
        when(cityDao.searchByName(name, limit)).thenReturn(cityList);
        List<City> c = cityService.searchByName(name, limit);
        assertNotNull(c);
        assertEquals(cityList.get(0).getId(),c.get(0).getId());
        assertEquals(cityList.get(1).getId(),c.get(1).getId());
    }

}

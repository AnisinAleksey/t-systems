package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.OrderDao;
import com.tsystems.logiweb.dao.TruckDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.PersistenceManager;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.service.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FactoryDao.class,PersistenceManager.class})
public class OrderServiceTest {
    private OrderDao orderDao;
    private OrderService orderService = new OrderServiceImpl();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(FactoryDao.class);
        PowerMockito.mockStatic(PersistenceManager.class);
        PersistenceManager persistenceManager = mock(PersistenceManager.class);
        PowerMockito.when(PersistenceManager.getInstance()).thenReturn(persistenceManager);
        EntityManager em = mock(EntityManager.class);
        orderDao = mock(OrderDao.class);
        when(persistenceManager.borrowEntityManager()).thenReturn(em);
        PowerMockito.when(FactoryDao.createOrderDao(em)).thenReturn(orderDao);
    }

    @Test
    public void getOrderByNumberTest() throws Exception {
        Long number = 2945378923l;
        Long id = 90l;
        Order order = new Order();
        order.setId(id);
        when(orderDao.getOrderByNumber(number)).thenReturn(order);
        Order o = orderService.getOrderByNumber(number);
        assertNotNull(o);
        assertEquals(id,o.getId());
    }

    @Test
    public void isMadeTest() throws Exception {
        boolean state = false;
        Long id = 45l;
        Order order = new Order();
        order.setId(id);
        when(orderDao.isMade(order)).thenReturn(state);
        boolean s = orderService.isMade(order);
        assertNotNull(s);
        assertEquals(state,s);
    }
}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.TruckDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.PersistenceManager;
import com.tsystems.logiweb.entities.Truck;
import com.tsystems.logiweb.service.TruckService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Юлия on 20.06.2016.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({FactoryDao.class,PersistenceManager.class})
public class TruckServiceTest {

    private TruckDao truckDao;

    private TruckService truckService = new TruckServiceImpl();


    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(FactoryDao.class);
        PowerMockito.mockStatic(PersistenceManager.class);
        PersistenceManager persistenceManager = mock(PersistenceManager.class);
        PowerMockito.when(PersistenceManager.getInstance()).thenReturn(persistenceManager);
        EntityManager em = mock(EntityManager.class);
        truckDao = mock(TruckDao.class);
        when(persistenceManager.borrowEntityManager()).thenReturn(em);
        PowerMockito.when(FactoryDao.createTruckDao(em)).thenReturn(truckDao);
    }


    @Test
    public void getTruckByRegnumberTest() throws Exception {
        String regNumber = "ab11111";
        Long id = 11L;
        Truck truck = new Truck();
        truck.setId(id);
        when(truckDao.getTruckByRegnumber(regNumber)).thenReturn(truck);
        Truck tr = truckService.getTruckByRegnumber(regNumber);
        assertNotNull(tr);
        assertEquals(id,tr.getId());
    }

}

package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.DriverDao;
import com.tsystems.logiweb.dao.TruckDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.PersistenceManager;
import com.tsystems.logiweb.entities.Driver;
import com.tsystems.logiweb.service.DriverService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FactoryDao.class,PersistenceManager.class})
public class DriverServiceTest {
    private DriverDao driverDao;
    private DriverService driverService = new DriverServiceImpl();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(FactoryDao.class);
        PowerMockito.mockStatic(PersistenceManager.class);
        PersistenceManager persistenceManager = mock(PersistenceManager.class);
        PowerMockito.when(PersistenceManager.getInstance()).thenReturn(persistenceManager);
        EntityManager em = mock(EntityManager.class);
        driverDao = mock(DriverDao.class);
        when(persistenceManager.borrowEntityManager()).thenReturn(em);
        PowerMockito.when(FactoryDao.createDriverDao(em)).thenReturn(driverDao);
    }

    @Test
    public void getDriverByLicenceTest() throws Exception {
        Long licence = 12111l;
        Long id = 45l;
        Driver driver = new Driver();
        driver.setId(45l);
        when(driverDao.getDriverByLicence(licence)).thenReturn(driver);
        Driver d = driverService.getDriverByLicence(licence);
        assertNotNull(d);
        assertEquals(d.getId(),id);
    }

    @Test
    public void getCoDriversByLicence() throws Exception {
        Long licence = 34785l;
        Long licence1 = 89251l;
        Long licence2 = 74563l;
        Long id = 11l;
        //Long id1 = 12l;
       // Long id2 = 13l;
        Driver driver = new Driver();
        driver.setId(id);
        //Driver driver1 = new Driver();
        //Driver driver2 = new Driver();
        //driver1.setId(id1);
        //driver2.setId(id2);
        //driver1.setP_number(licence1);
        //driver2.setP_number(licence2);
        List<Long> list = new ArrayList<>();
        list.add(licence1);
        list.add(licence2);
        when(driverDao.getCoDriversByLicence(licence)).thenReturn(list);
        List<Long> l = driverService.getCoDriversByLicence(licence);
        assertNotNull(l);
        assertEquals(list ,l);
        //assertEquals(list.get(1),l.get(1));

    }
}

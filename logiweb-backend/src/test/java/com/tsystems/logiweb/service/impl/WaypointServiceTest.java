package com.tsystems.logiweb.service.impl;

import com.tsystems.logiweb.dao.TruckDao;
import com.tsystems.logiweb.dao.WaypointDao;
import com.tsystems.logiweb.dao.factoryDao.FactoryDao;
import com.tsystems.logiweb.engine.PersistenceManager;
import com.tsystems.logiweb.entities.Order;
import com.tsystems.logiweb.entities.Waypoint;
import com.tsystems.logiweb.service.WaypointService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FactoryDao.class,PersistenceManager.class})
public class WaypointServiceTest {

    private WaypointDao waypointDao;

    private WaypointService waypointService = new WaypointServiceImpl();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(FactoryDao.class);
        PowerMockito.mockStatic(PersistenceManager.class);
        PersistenceManager persistenceManager = mock(PersistenceManager.class);
        PowerMockito.when(PersistenceManager.getInstance()).thenReturn(persistenceManager);
        EntityManager em = mock(EntityManager.class);
        waypointDao = mock(WaypointDao.class);
        when(persistenceManager.borrowEntityManager()).thenReturn(em);
        PowerMockito.when(FactoryDao.createWaypointDao(em)).thenReturn(waypointDao);
    }

    @Test
    public void getListOfWaypoint() throws Exception {
        Long orderId = 34l;
        Long waypointId = 89l;
        Long waypointId2 =90l;
        Order order = new Order();
        Waypoint waypoint = new Waypoint();
        Waypoint waypoint2 = new Waypoint();
        order.setId(orderId);
        waypoint.setId(waypointId);
       // waypoint.setOrder(order);
        waypoint2.setId(waypointId2);
        //waypoint2.setOrder(order);
        List<Waypoint> list = new ArrayList<>();
        list.add(waypoint);
        list.add(waypoint2);
        when(waypointDao.getListOfWaypoint(order)).thenReturn(list);
        List<Waypoint> l = waypointService.getListOfWaypoint(order);
        assertNotNull(l);
        assertEquals(l.get(0).getId(), list.get(0).getId());
        assertEquals(l.get(1).getId(), list.get(1).getId());
    }
}
